This is an eBaoTech Java project to automatically generate recurrent reports.

Before running is necessary to configure the file config.properties and the log4j.xml level.

How to execute:
java -jar recurrent.jar &lt;report&gt; &lt;parameters&gt;

&lt;report&gt; is the number of the reports you want, separated by comma (,) and without spaces:<br/>
1 - Boletos<br/>
2 - Debitos automaticos<br/>
3 - IOF<br/>
4 - Baixas manuais<br/>
5 - Cancelamento por inadiplência<br/>

&lt;parameters&gt; are OPTIONAL, they are start and end datetime of IOF report, for example:<br/>
"12/05/2018 10:00:00" "21/05/2018 23:59:59"

Example:<br/>
java -jar recurrent.jar 1,2,3,4,5 "12/05/2018 10:00:00" "21/05/2018 23:59:59"