/**
 * 
 */
package com.ebao.gs.recurrent.jdbc;

/**
 * @author Matheus Lopes
 * 
 *
 * Database Info
 */
public class DatabaseInfo {
	private static String database = null;
	private static String user = null;
	private static String pass = null;
	private static String session = null;
	private static String outputDir = null;
	
	public static String getDatabase() {
		return database;
	}
	public static void setDatabase(String database) {
		DatabaseInfo.database = database;
	}
	public static String getUser() {
		return user;
	}
	public static void setUser(String user) {
		DatabaseInfo.user = user;
	}
	public static String getPass() {
		return pass;
	}
	public static void setPass(String pass) {
		DatabaseInfo.pass = pass;
	}
	public static String getSession() {
		return session;
	}
	public static void setSession(String session) {
		DatabaseInfo.session = session;
	}
	public static String getOutputDir() {
		return outputDir;
	}
	public static void setOutputDir(String outputDir) {
		DatabaseInfo.outputDir = outputDir;
	}
}
