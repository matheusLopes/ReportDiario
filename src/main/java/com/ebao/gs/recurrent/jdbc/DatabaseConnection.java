package com.ebao.gs.recurrent.jdbc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class DatabaseConnection {

	public static final String ALGORITHM = "AES";

	/**
	 * decrypt a value
	 * 
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	public String decrypt(String message, File keyFile) throws GeneralSecurityException, IOException {
		SecretKeySpec sks = getSecretKeySpec(keyFile);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, sks);
		byte[] decrypted = cipher.doFinal(hexStringToByteArray(message));
		return new String(decrypted);
	}

	private SecretKeySpec getSecretKeySpec(File keyFile) throws NoSuchAlgorithmException, IOException {
		byte[] key = readKeyFile(keyFile);
		SecretKeySpec sks = new SecretKeySpec(key, ALGORITHM);
		return sks;
	}

	private byte[] readKeyFile(File keyFile) throws FileNotFoundException {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(keyFile).useDelimiter("\\Z");
		String keyValue = scanner.next();
		scanner.close();
		return hexStringToByteArray(keyValue);
	}

	private byte[] hexStringToByteArray(String s) {
		byte[] b = new byte[s.length() / 2];
		for (int i = 0; i < b.length; i++) {
			int index = i * 2;
			int v = Integer.parseInt(s.substring(index, index + 2), 16);
			b[i] = (byte) v;
		}
		return b;
	}

	public Connection connect(String database, String user, String pwd) throws SQLException, GeneralSecurityException, IOException, ClassNotFoundException {
		Class.forName("oracle.jdbc.OracleDriver");
		
		String decryptedPwd = decrypt(pwd, new File("conf/config.key"));
		return DriverManager.getConnection("jdbc:oracle:thin:@//" + database, user, decryptedPwd);
	}
}
