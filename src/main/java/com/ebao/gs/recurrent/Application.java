package com.ebao.gs.recurrent;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ebao.gs.recurrent.jdbc.DatabaseInfo;
import com.ebao.gs.recurrent.reports.*;

public class Application {
	private static Logger logger = LogManager.getLogger(Application.class.getName());
	private static final String VERSION = "1.2";

	public static void main(String[] args) throws Exception {
		if (args.length < 1) {
			logger.error("Low number of arguments!");
			System.exit(1);
		}
		logger.info("Daily Report Project " + VERSION);
		logger.info("Application started: " + new Date());

		loadProperties();

		int[] reports = stringToIntArray(args[0]);
		@SuppressWarnings("rawtypes")
		CompletableFuture[] cF = new CompletableFuture[reports.length];

		for(int i = 0; i < reports.length; i++) {
			int report = reports[i];
			
			switch (report) {
				case 1:
					/* generate Boletos report */
					if(args.length == 3) {
						String startDate = args[1];
						String endDate = args[2];
						
						CompletableFuture<?> c1 = CompletableFuture.runAsync(() -> {
							try {
								ReportBoletos.generateReport(startDate, endDate);
							}catch(Exception e) {
								logger.error(e);
							}
						});	
						
						cF[i] = c1;
					} else {
						logger.error("Only 3 parameters (report, start date and end date) are allowed for boleto report!");
						System.exit(1);
					}
					
					break;
					
				case 2:
					/* generate Debit report */
					CompletableFuture<?> c2 = CompletableFuture.runAsync(() -> {
						try {
							ReportDebit.generateReport(null, null);
						}catch(Exception e) {
							logger.error(e);
						}
					});	
					
					cF[i] = c2;
					break;	
					
				case 3:
					/* generate IOF report */
					CompletableFuture<?> c3 = CompletableFuture.runAsync(() -> {
						try {
							if (args.length == 3) {
								ReportIOF.generateReport(args[1], args[2]);
							} else {
								ReportIOF.generateReport(null, null);
							}
						}catch(Exception e) {
							logger.error(e);
						}
					});	
					
					cF[i] = c3;
					break;
					
				case 4:
					/* generate Baixas Manuais report */
					CompletableFuture<?> c4 = CompletableFuture.runAsync(() -> {
						try {
							ReportBaixasManuais.generateReport();
						}catch(Exception e) {
							logger.error(e);
						}
					});	
					
					cF[i] = c4;
					break;
				case 5:
					/* generate Cancelamento por inadiplencia report */
					CompletableFuture<?> c5 = CompletableFuture.runAsync(() -> {
						try {
							ReportCancInadimplencia.generateReport();
						}catch(Exception e) {
							logger.error(e);
						}
					});	
					
					cF[i] = c5;
					break;
				default:
					logger.error(
							"Wrong report id: 1 - Boletos; 2 - Debitos automaticos; 3 - IOF; 4 - Baixas manuais; 5 - Cancelamento inadimplencia");
					break;
			}
		}

		CompletableFuture.allOf(cF).join();//blocking call

		logger.info("Application ended: " + new Date());
	}

	private static void loadProperties() throws FileNotFoundException, IOException {
		String appConfigPath = "conf/config.properties";
		Properties appProps = new Properties();
		appProps.load(new FileInputStream(appConfigPath));

		DatabaseInfo.setDatabase(appProps.getProperty("database"));
		DatabaseInfo.setUser(appProps.getProperty("user"));
		DatabaseInfo.setPass(appProps.getProperty("pass"));
		DatabaseInfo.setSession(appProps.getProperty("session"));
		DatabaseInfo.setOutputDir(appProps.getProperty("outputDir"));
	}

	private static int[] stringToIntArray(String values) {
		String[] aux = values.split(",");
		int[] array = new int[aux.length];
		for(int i = 0; i < aux.length; i++)
			array[i] = Integer.parseInt(aux[i]);

		return array;
	}
}