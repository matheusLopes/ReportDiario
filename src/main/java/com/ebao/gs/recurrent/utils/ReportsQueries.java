package com.ebao.gs.recurrent.utils;

public class ReportsQueries {

	public static String queryNormais_Boletos(String startDate, String endDate) {
		return " SELECT" +
				"   '033 - Santander'                                                                                                              AS \"BANK\"," +
				"   f.POLICY_NO                                                                                                                    AS \"APOLICE\"," +
				"   fd.ARAP_REF_NO                                                                                                                 AS \"DEBIT NOTE\"," +
				"   b.BOLETO_NO                                                                                                                    AS \"BOLETO\"," +
				"   (SELECT COUNT(1)" +
				"    FROM T_CTS_BOLETO B2" +
				"    WHERE B2.FEE_ID = fd.FEE_ID)                                                                                                  AS \"REPRINT?\"," +
				"   (SELECT MIN(B2.INSERT_TIMESTAMP)" +
				"    FROM T_CTS_BOLETO B2" +
				"    WHERE B2.FEE_ID = fd.FEE_ID)                                                                                                  AS \"GERAÇAO 1º BOLETO\"," +
				"   (SELECT MAX(B2.INSERT_TIMESTAMP)" +
				"    FROM T_CTS_BOLETO B2" +
				"    WHERE B2.FEE_ID = fd.FEE_ID)                                                                                                  AS \"GERAÇAO ÚLTIMO BOLETO\"," +
				"   b.INSERT_TIMESTAMP                                                                                                             AS \"DATA GERAÇAO BOLETO PAGO\"," +
				"   bs.STATUS_DESC                                                                                                                 AS \"STATUS DO BOLETO\"," +
				"   b.AMOUNT                                                                                                                       AS \"VALOR ORIGINAL\"," +
				"   TO_NUMBER(RU.PAID_VALUE / 100)                                                                                                 AS \"VALOR PAGO\"," +
				"   TO_NUMBER(RU.PAID_VALUE / 100) - B.AMOUNT                                                                                      AS \"JUROS ACRESC\"," +
				"   CAST(((b.amount * 0.02) + (b.amount * 0.01) * (TO_DATE(RU.PAY_DATE, 'ddmmyyyy') - B.REPRINT_DUE_DATE) / 30) AS DECIMAL(10, 2)) AS \"JUROS ESPERADO\"," +
				"   fd.BALANCE                                                                                                                     AS \"SALDO EM ABERTO\"," +
				"   st.status_name                                                                                                                 AS \"STATUS DA PARCELA\"," +
				"   (CASE WHEN fd.balance <> 0" +
				"     THEN 'Em Aberto'" +
				"    ELSE 'Pago' END)                                                                                                              AS \"STATUS SALDO\"," +
				"   b.BP_SEQ                                                                                                                       AS \"PARCELA\"," +
				"   b.REPRINT_DUE_DATE                                                                                                             AS \"VENCIMENTO\"," +
				"   TO_DATE(RU.PAY_DATE, 'ddmmyyyy')                                                                                               AS \"PAGO EM\"," +
				"   TO_DATE(RU.EFF_DATE, 'ddmmyyyy')                                                                                               AS \"DATA DO CREDITO\"," +
				"   rh.DOC                                                                                                                         AS \"CONVENIO\"," +
				"   b.IOF," +
				"   b.PER_IOF                                                                                                                      AS \"PERCENTUAL IOF\"," +
				"   rs.STATUS_NAME                                                                                                                 AS \"DESCRIÇÃO DO ERRO\"," +
				"   r.BATCH_RUN_ID                                                                                                                 AS \"RUN ID\"," +
				"   r.UPDATE_TIMESTAMP                                                                                                             AS \"PROCESSADO EM\"," +
				"   (SELECT COUNT(1)" +
				"    FROM T_CTS_BOLETO_SANT_RET_SEGT RT2" +
				"    WHERE RT2.BOLETO_NO = RT.BOLETO_NO AND RT2.MOVEMENT_CODE IN (6, 17))                                                          AS QUANTIDADE_PAGOS," +
				"   TO_NUMBER(ru.iof_value / 100)                                                                                                  AS IOF_RETIDO_SANTANDER," +
				"   TO_NUMBER(ru.adjustment_value / 100)                                                                                           AS DESCONTO_AGRAVO_SANTANDER," +
				"   cbr.iof_bank_withholding                                                                                                       AS FLAG_BAIXA" +
				" FROM T_CTS_BOLETO_SANT_RETURN r" +
				"  	join T_CTS_BOLETO_SANT_RET_SEGT rt ON r.RETURN_ID = rt.RETURN_ID" +
				"  	join T_CTS_BCP_BOLETO_RETURN_STATUS rs ON rs.STATUS_ID = rt.STATUS" +
				"  	join T_CTS_BOLETO_SANT_RET_SEGU ru ON rt.RETURN_ID = ru.RETURN_ID AND rt.SEG_SEQ = ru.SEG_SEQ" +
				"  	join T_CTS_BOLETO b ON b.BOLETO_ID = rt.BOLETO_NO" +
				"  	join T_CTS_BOLETO_STATUS bs ON b.BOLETO_STATUS = bs.STATUS_ID" +
				"  	join T_CTS_BOLETO_FEE_MAPPING fm ON fm.BOLETO_ID = b.BOLETO_ID" +
				"  	join T_CTS_BOLETO_SANT_RET_HEADBAT rh ON rt.RETURN_ID = rh.RETURN_ID" +
				"  	join T_BCP_FEE_DETAIL fd ON fd.FEE_ID = fm.FEE_ID" +
				"  	join T_BCP_FEE f ON f.TRANS_ID = fd.TRANS_ID" +
				"  	join T_BCP_CFG_ARAP_STATUS st ON fd.ARAP_STATUS = st.STATUS_ID" +
				"  	left join T_BCP_RELATION br ON br.DEBIT_ID = fd.FEE_ID" +
				"  	left join T_CTS_BCP_RELATION cbr ON br.RELATION_ID = cbr.RELATION_ID" +
				" WHERE 1 = 1" +
				"   AND r.UPDATE_TIMESTAMP BETWEEN to_date('" + startDate + "', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" + endDate + "', 'dd/MM/yyyy HH24:MI:SS')" +
				"   AND rt.STATUS <> 1" +
				"   AND fd.ARAP_STATUS <> 5" +
				"   AND rt.MOVEMENT_CODE IN (6, 17)" +
				" ORDER BY 1";
	}
	public static String getCancelados_Boletos(String startDate, String endDate) {
		return 	" SELECT" +
				"    '033 - Santander'                                                                                                              AS BANK," + 
				"    f.POLICY_NO                                                                                                                    AS APOLICE," + 
				"    fd.ARAP_REF_NO                                                                                                                 AS \"DEBIT NOTE\"," + 
				"    b.BOLETO_NO                                                                                                                    AS \"BOLETO\"," + 
				"    (SELECT COUNT(1)" +
				"     FROM T_CTS_BOLETO B2" +
				"     WHERE B2.FEE_ID = fd.FEE_ID)                                                                                                  AS \"REPRINT?\"," + 
				"    (SELECT MIN(B2.INSERT_TIMESTAMP)" +
				"     FROM T_CTS_BOLETO B2" +
				"     WHERE B2.FEE_ID = fd.FEE_ID)                                                                                                  AS \"GERAÇAO 1º BOLETO\"," + 
				"    (SELECT MAX(B2.INSERT_TIMESTAMP)" +
				"     FROM T_CTS_BOLETO B2" +
				"     WHERE B2.FEE_ID = fd.FEE_ID)                                                                                                  AS \"GERAÇAO ÚLTIMO BOLETO\"," + 
				"    b.INSERT_TIMESTAMP                                                                                                             AS \"DATA GERAÇAO BOLETO PAGO\"," + 
				"    bs.STATUS_DESC                                                                                                                 AS \"STATUS DO BOLETO\"," + 
				"    b.AMOUNT                                                                                                                       AS \"VALOR ORIGINAL\"," + 
				"    TO_NUMBER(RU.PAID_VALUE / 100)                                                                                                 AS \"VALOR PAGO\"," + 
				"    TO_NUMBER(RU.PAID_VALUE / 100) - B.AMOUNT                                                                                      AS \"JUROS ACRESC\"," + 
				"    CAST(((b.amount * 0.02) + (b.amount * 0.01) * (TO_DATE(RU.PAY_DATE, 'ddmmyyyy') - B.REPRINT_DUE_DATE) / 30) AS DECIMAL(10, 2)) AS \"JUROS ESPERADO\"," + 
				"    fd.BALANCE                                                                                                                     AS \"SALDO EM ABERTO\"," + 
				"    st.status_name                                                                                                                 AS \"STATUS DA PARCELA\"," + 
				"    (CASE WHEN fd.balance <> 0" +
				"      THEN 'Em Aberto'" +
				"     ELSE 'Pago' END)                                                                                                              AS \"STATUS SALDO\"," + 
				"    b.BP_SEQ                                                                                                                       AS PARCELA," + 
				"    b.REPRINT_DUE_DATE                                                                                                             AS VENCIMENTO," + 
				"    TO_DATE(RU.PAY_DATE, 'ddmmyyyy')                                                                                               AS \"PAGO EM\"," + 
				"    TO_DATE(RU.EFF_DATE, 'ddmmyyyy')                                                                                               AS \"DATA DO CREDITO\"," + 
				"    rh.DOC                                                                                                                         AS CONVENIO," + 
				"    b.IOF," +
				"    b.PER_IOF                                                                                                                      AS \"PERCENTUAL IOF\"," + 
				"    rs.STATUS_NAME                                                                                                                 AS \"DESCRIÇÃO DO ERRO\"," + 
				"    r.BATCH_RUN_ID," +
				"    r.UPDATE_TIMESTAMP                                                                                                             AS \"PROCESSADO EM\"," + 
				"    TO_NUMBER(ru.iof_value / 100)                                                                                                  AS IOF_RETIDO_SANTANDER," + 
				"    TO_NUMBER(ru.adjustment_value / 100)                                                                                           AS DESCONTO_AGRAVO_SANTANDER" + 
				" FROM T_CTS_BOLETO_SANT_RETURN r" +
				"    join T_CTS_BOLETO_SANT_RET_SEGT rt ON r.RETURN_ID = rt.RETURN_ID" +
				"    join T_CTS_BCP_BOLETO_RETURN_STATUS rs ON rs.STATUS_ID = rt.STATUS" +
				"    join T_CTS_BOLETO_SANT_RET_SEGU ru ON rt.RETURN_ID = ru.RETURN_ID AND rt.SEG_SEQ = ru.SEG_SEQ" +
				"    join T_CTS_BOLETO b ON b.BOLETO_ID = rt.BOLETO_NO" +
				"    join T_CTS_BOLETO_STATUS bs ON b.BOLETO_STATUS = bs.STATUS_ID" +
				"    join T_CTS_BOLETO_FEE_MAPPING fm ON fm.BOLETO_ID = b.BOLETO_ID" +
				"    join T_CTS_BOLETO_SANT_RET_HEADBAT rh ON rt.RETURN_ID = rh.RETURN_ID" +
				"    join T_BCP_FEE_DETAIL fd ON fd.FEE_ID = fm.FEE_ID" +
				"    join T_BCP_FEE f ON f.TRANS_ID = fd.TRANS_ID" +
				"    join T_BCP_CFG_ARAP_STATUS st ON fd.ARAP_STATUS = st.STATUS_ID" +
				"    left join T_BCP_RELATION br ON br.DEBIT_ID = fd.FEE_ID" +
				"    left join T_CTS_BCP_RELATION cbr ON br.RELATION_ID = cbr.RELATION_ID" +
				" WHERE 1 = 1" +
				"    AND r.UPDATE_TIMESTAMP BETWEEN to_date('" + startDate + "', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" + endDate + "', 'dd/MM/yyyy HH24:MI:SS')" +
				"    AND rt.STATUS <> 1" +
				"    and fd.ARAP_STATUS = 5" +
				"    and rt.MOVEMENT_CODE IN (6, 17)";	
	}
	public static String getPrePag_Boletos(String startDate, String endDate) {
		return "SELECT" +
				"    '033 - Santander'                                                                                                              AS BANK," + 
				"    pg.QUOTE_NO                                                                                                                    AS \"NUMERO DA PROPOSTA\"," + 
				"    pg.POLICY_NO                                                                                                                   AS APOLICE," + 
				"    i.INDI_PARTY_NAME                                                                                                              AS SEGURADO," + 
				"    b.BOLETO_NO                                                                                                                    AS BOLETO," + 
				"    TO_CHAR(B.REPRINT_DUE_DATE, 'DD/MM/YYYY')                                                                                      AS VENCIMENTO," + 
				"    b.AMOUNT                                                                                                                       AS \"VALOR ORIGINAL\"," + 
				"    TO_NUMBER(RU.PAID_VALUE / 100)                                                                                                 AS \"VALOR PAGO\"," + 
				"    CAST(((b.amount * 0.02) + (b.amount * 0.01) * (TO_DATE(RU.PAY_DATE, 'ddmmyyyy') - B.REPRINT_DUE_DATE) / 30) AS DECIMAL(10, 2)) AS \"JUROS ACRESC\"," + 
				"    bs.STATUS_DESC                                                                                                                 AS STATUS," + 
				"    TO_CHAR(B.LAST_PRINT_DATE, 'DD/MM/YYYY')                                                                                       AS \"EMITIDO EM\"," + 
				"    TO_DATE(RU.PAY_DATE, 'ddmmyyyy')                                                                                               AS \"PAGO EM\"," + 
				"    TO_DATE(RU.EFF_DATE, 'ddmmyyyy')                                                                                               AS \"DATA DO CREDITO\"," + 
				"    rh.DOC                                                                                                                         AS CONVENIO," + 
				"    b.IOF," +
				"    b.PER_IOF                                                                                                                      AS \"PERCENTUAL DE IOF\"," + 
				"    rs.STATUS_NAME                                                                                                                 AS \"DESCRIÇÃO DO ERRO\"," + 
				"    r.BATCH_RUN_ID," +
				"    r.UPDATE_TIMESTAMP                                                                                                             AS \"PROCESSADO EM\"," + 
				"    TO_NUMBER(ru.iof_value / 100)                                                                                                  AS IOF_RETIDO_SANTANDER," + 
				"    TO_NUMBER(ru.adjustment_value / 100)                                                                                           AS DESCONTO_AGRAVO_SANTNADER" + 
				" FROM T_CTS_BOLETO_SANT_RETURN r" +
				"    join T_CTS_BOLETO_SANT_RET_SEGT rt ON r.RETURN_ID = rt.RETURN_ID" +
				"    join T_CTS_BCP_BOLETO_RETURN_STATUS rs ON rs.STATUS_ID = rt.STATUS" +
				"    join T_CTS_BOLETO_SANT_RET_SEGU ru ON rt.RETURN_ID = ru.RETURN_ID AND rt.SEG_SEQ = ru.SEG_SEQ" +
				"    join T_CTS_BOLETO b ON b.BOLETO_ID = rt.BOLETO_NO" +
				"    join T_CTS_BOLETO_STATUS bs ON b.BOLETO_STATUS = bs.STATUS_ID" +
				"    join T_CTS_BOLETO_FEE_MAPPING fm ON fm.BOLETO_ID = b.BOLETO_ID" +
				"    join T_CTS_BOLETO_SANT_RET_HEADBAT rh ON rt.RETURN_ID = rh.RETURN_ID" +
				"    left join T_PTY_INDI i ON b.PAYER_ID = i.PTY_ID" +
				"    left join T_BCP_FEE_DETAIL fd ON fd.FEE_ID = fm.FEE_ID" +
				"    left join T_BCP_FEE f ON f.TRANS_ID = fd.TRANS_ID" +
				"    left join T_POLICY_GENERAL pg ON pg.POLICY_ID = f.POLICY_ID" +
				"    left join T_BCP_CFG_ARAP_STATUS st ON fd.ARAP_STATUS = st.STATUS_ID" +
				" WHERE 1 = 1" +
				"    AND pg.POLICY_NO IS NULL" +
				"    AND r.UPDATE_TIMESTAMP BETWEEN to_date('" + startDate + "', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" + endDate + "', 'dd/MM/yyyy HH24:MI:SS')" +
				"    AND rt.STATUS <> 1" +
				" ORDER BY 1";
	}
	public static String getNIdentif_Boletos(String startDate, String endDate) {
		return "SELECT" +
				"    '033 - Santander'                                          AS BANK," + 
				"    rt.our_number                                              AS \"BOLETO RECEBIDO\"," + 
				"    (CASE WHEN fd.ARAP_REF_NO IS NULL AND B.policy_id IS NOT NULL" +
				"      THEN 'SIM'" +
				"     WHEN B.policy_id IS NULL" +
				"       THEN 'N/A'" +
				"     ELSE 'NÃO' END)                                           AS \"PRE-PAGAMENTO\"," + 
				"    pg.QUOTE_NO                                                AS \"NUMERO DA PROPOSTA\"," + 
				"    (CASE WHEN pg.quote_no IS NULL AND rt.boleto_no IN (SELECT B.BOLETO_id" +
				"                                                        FROM t_cts_boleto b" +
				"                                                        WHERE b.bank_code = 33 AND b.amount > 0)" +
				"      THEN 'Apólice do Transact sem Policy_id'" +
				"     ELSE 'Erro Número de Boleto - Contato T.I Santander' END) AS APOLICE," + 
				"    i.INDI_PARTY_NAME                                          AS SEGURADO," + 
				"    TO_NUMBER(RU.PAID_VALUE) / 100                             AS \"VALOR PAGO\"," + 
				"    TO_DATE(RU.PAY_DATE, 'ddmmyyyy')                           AS \"PAGO EM\"," + 
				"    TO_DATE(RU.EFF_DATE, 'ddmmyyyy')                           AS \"DATA DO CREDITO\"," + 
				"    b.BOLETO_NO                                                AS \"BOLETO ORIGINAL\"," + 
				"    b.AMOUNT                                                   AS \"VALOR ORIGINAL\"," + 
				"    b.DUE_DATE                                                 AS \"VENCIMENTO ORIGINAL\"," + 
				"    fd.ARAP_REF_NO                                             AS \"DEBIT NOTE\"," + 
				"    fd.BALANCE                                                 AS \"SALDO EM ABERTO\"," + 
				"    st.STATUS_NAME                                             AS STATUS," + 
				"    (CASE WHEN fd.balance <> fd.amount" +
				"      THEN 'Pago'" +
				"     ELSE 'Em Aberto' END)                                     AS \"STATUS SALDO\"," + 
				"    b.PER_IOF                                                  AS \"PERCENTUAL DE IOF\"," + 
				"    rs.STATUS_NAME                                             AS \"DESCRICAO DO ERRO\"," + 
				"    r.BATCH_RUN_ID," +
				"    r.UPDATE_TIMESTAMP                                         AS \"PROCESSADO EM\"," + 
				"    TO_NUMBER(ru.iof_value / 100)                              AS IOF_RETIDO_SANTANDER," + 
				"    TO_NUMBER(ru.adjustment_value / 100)                       AS DESCONTO_AGRAVO_SANTNADER," + 
				"    cbr.iof_bank_withholding                                   AS FLAG_BAIXA" +
				" FROM T_CTS_BOLETO_SANT_RETURN r" +
				"    join T_CTS_BOLETO_SANT_RET_SEGT rt ON r.RETURN_ID = rt.RETURN_ID" +
				"    join T_CTS_BCP_BOLETO_RETURN_STATUS rs ON rs.STATUS_ID = rt.STATUS" +
				"    join T_CTS_BOLETO_SANT_RET_SEGU ru ON rt.RETURN_ID = ru.RETURN_ID AND rt.SEG_SEQ = ru.SEG_SEQ" +
				"    join T_CTS_BOLETO b ON b.BOLETO_ID = rt.BOLETO_NO" +
				"    join T_CTS_BOLETO_STATUS bs ON b.BOLETO_STATUS = bs.STATUS_ID" +
				"    join T_CTS_BOLETO_FEE_MAPPING fm ON fm.BOLETO_ID = b.BOLETO_ID" +
				"    join T_CTS_BOLETO_SANT_RET_HEADBAT rh ON rt.RETURN_ID = rh.RETURN_ID" +
				"    left join T_PTY_INDI i ON b.PAYER_ID = i.PTY_ID" +
				"    left join T_BCP_FEE_DETAIL fd ON fd.FEE_ID = fm.FEE_ID" +
				"    left join T_BCP_FEE f ON f.TRANS_ID = fd.TRANS_ID" +
				"    left join T_POLICY_GENERAL pg ON pg.POLICY_ID = f.POLICY_ID" +
				"    left join T_BCP_CFG_ARAP_STATUS st ON fd.ARAP_STATUS = st.STATUS_ID" +
				"    left join T_BCP_RELATION br ON br.DEBIT_ID = fd.FEE_ID" +
				"    left join T_CTS_BCP_RELATION cbr ON br.RELATION_ID = cbr.RELATION_ID" +
				" WHERE 1 = 1" +
				"    AND rt.STATUS IN (0, 9)" +
				"    AND rt.MOVEMENT_CODE IN (6, 17)" +
				"    AND r.UPDATE_TIMESTAMP BETWEEN to_date('" + startDate + "', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" + endDate + "', 'dd/MM/yyyy HH24:MI:SS')" +
				" ORDER BY 1";
	}
	public static String getGeral_Boletos(String startDate, String endDate) {
		return 	"SELECT" +
				"    '033 - Santander'                AS BANK," + 
				"    LPAD(RT.OUR_NUMBER, 13, '0')     AS BOLETO," + 
				"    TO_DATE(RU.PAY_DATE, 'ddmmyyyy') AS \"PAGO EM\"," + 
				"    TO_DATE(RU.EFF_DATE, 'ddmmyyyy') AS \"DATA DO CREDITO\"," + 
				"    TO_NUMBER(RU.PAID_VALUE / 100)   AS \"VALOR PAGO\"," + 
				"    rn.INSERT_TIMESTAMP              AS PROCESSAMENTO," + 
				"    rs.STATUS_NAME                   AS \"DESCRICAO ERRO\"," + 
				"    rn.BATCH_RUN_ID," +
				"    rn.UPDATE_TIMESTAMP              AS \"PROCESSADO EM\"" + 
				" FROM T_CTS_BOLETO_SANT_RET_SEGT RT" +
				"    join T_CTS_BOLETO_SANT_RETURN R ON R.RETURN_ID = RT.RETURN_ID" +
				"    join T_CTS_BOLETO_SANT_RET_SEGU RU ON RT.RETURN_ID = RU.RETURN_ID AND RT.SEG_SEQ = RU.SEG_SEQ" +
				"    join T_CTS_BOLETO_SANT_RET_HEADBAT RH ON RT.RETURN_ID = RH.RETURN_ID" +
				"    join T_CTS_BOLETO_SANT_RETURN RN ON RT.RETURN_ID = RN.RETURN_ID" +
				"    join T_CTS_BCP_BOLETO_RETURN_STATUS RS ON RS.STATUS_ID = Rt.STATUS" +
				" WHERE rt.STATUS <> 1" +
				"    AND rt.MOVEMENT_CODE IN (6, 17)" +
				"    AND R.UPDATE_TIMESTAMP BETWEEN to_date('" + startDate + "', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" + endDate + "', 'dd/MM/yyyy HH24:MI:SS')";	
	}
	public static String getSumHsbc_Boletos(String startDate, String endDate) {
		return 	" select" +
				"   'Total' AS status_name," +
				"   count(1)" +
				" FROM T_CTS_BCP_HSBC_BOLETO_RETURN RN" +
				"   join T_CTS_BCP_BOLETO_RETURN_STATUS RS ON RS.STATUS_ID = RN.STATUS_ID" +
				" WHERE RN.UPDATE_TIMESTAMP BETWEEN to_date('" + startDate + "', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" + endDate + "', 'dd/MM/yyyy HH24:MI:SS')" +
				"union all " +
				" select" +
				"   rs.status_name," +
				"   count(1)" +
				" FROM T_CTS_BCP_HSBC_BOLETO_RETURN RN" +
				"   join T_CTS_BCP_BOLETO_RETURN_STATUS RS ON RS.STATUS_ID = RN.STATUS_ID" +
				" WHERE RN.UPDATE_TIMESTAMP BETWEEN to_date('" + startDate + "', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" + endDate + "', 'dd/MM/yyyy HH24:MI:SS')" +
				" GROUP BY rs.status_name" +
				" ORDER BY 2";
	}
	public static String getSumSant_Boletos(String startDate, String endDate) {
		return " SELECT" +
				"    'Total' AS status_name," +
				"    count(1)" +
				" FROM T_CTS_BOLETO_SANT_RET_SEGT rt" +
				"    join T_CTS_BOLETO_SANT_RETURN rn ON rt.RETURN_ID = rn.RETURN_ID" +
				"    join T_CTS_BOLETO_SANT_RETURN r ON r.RETURN_ID = rt.RETURN_ID" +
				"    join T_CTS_BCP_BOLETO_RETURN_STATUS rs ON rs.STATUS_ID = rt.STATUS" +
				" WHERE 1 = 1" +
				"    AND r.UPDATE_TIMESTAMP BETWEEN to_date('" + startDate + "', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" + endDate + "', 'dd/MM/yyyy HH24:MI:SS')" +
				"    AND rt.MOVEMENT_CODE IN (6, 17)" +
				" UNION ALL " +
				" SELECT" +
				"    rs.status_name," +
				"    count(1)" +
				" FROM T_CTS_BOLETO_SANT_RET_SEGT rt" +
				"    join T_CTS_BOLETO_SANT_RETURN rn ON rt.RETURN_ID = rn.RETURN_ID" +
				"    join T_CTS_BOLETO_SANT_RETURN r ON r.RETURN_ID = rt.RETURN_ID" +
				"    join T_CTS_BCP_BOLETO_RETURN_STATUS rs ON rs.STATUS_ID = rt.STATUS" +
				" WHERE 1 = 1" +
				"    AND r.UPDATE_TIMESTAMP BETWEEN to_date('" + startDate + "', 'dd/MM/yyyy HH24:MI:SS') AND to_date('" + endDate + "', 'dd/MM/yyyy HH24:MI:SS')" +
				"    AND rt.MOVEMENT_CODE IN (6, 17)" +
				" GROUP BY rs.STATUS_NAME" +
				" ORDER BY 2";
	}
	public static String getDebit(String startDate, String endDate) {
		return 	"	SELECT DISTINCT F.POLICY_NO AS APOLICE ," + 
				"	AD.BANK_CODE," + 
				"	ACC_OFFICIAL AS AGENCIA_CONTA," + 
				"	DT.ARAP_REF_NO AS DEBIT_NOTE," + 
				"	DT.AMOUNT AS VALOR_ORIGINAL," + 
				"	AD.AR_AMOUNT  AS VALOR_DEBITADO," + 
				"	L.FILE_NAME AS NOME_DO_ARQUIVO," + 
				"	L.INSERT_TIMESTAMP AS PROCESSADO_EM," + 
				"	AD.SEQUENCIAL_NUMBER," + 
				"	AD.COMPANY_CODE," + 
				"	AD.AGREEMENT_CODE AS CONVENIO," + 
				"	AD.BANK_BRANCH_CODE," + 
				"	AD.AR_AMOUNT ," + 
				"	AD.DEBIT_DATE," + 
				"	AD.PH_BANK_BRANCH_CODE," + 
				"	A.PH_NAME," + 
				"	AD.RETURN_CODE," + 
				"	AD.RETURN_REASON_CODE," + 
				"	AD.insert_timestamp" + 
				"	FROM" + 
				"	T_CTS_INT_AUTODEBIT_RETURN AD" + 
				"	inner join T_CTS_INT_BCP_AUTODEBIT a" + 
				"	ON A.DEBIT_NOTE_NO = AD.DEBIT_NOTE_NO" + 
				"	INNER JOIN T_CTS_FILE_LOG L" + 
				"	ON L.FILE_ID = AD.FILE_ID" + 
				"	inner join T_BCP_FEE_DETAIL DT" + 
				"	on DT.ARAP_REF_NO = A.DEBIT_NOTE_NO" + 
				"	INNER JOIN T_BCP_FEE F" + 
				"	ON F.TRANS_ID = DT.TRANS_ID" + 
				"		  INNER JOIN T_POLICY_GENERAL P" + 
				"		  ON P.POLICY_ID = F.POLICY_ID" + 
				"		  INNER JOIN  T_POLICY_GEN_CUST C" + 
				"		  ON C.POLICY_ID = F.POLICY_ID" + 
				"		  inner join t_ptyr r" + 
				"		  on c.party_id = r.pty_id" + 
				"		  left outer join T_PTY_role_account racc" + 
				"		  on racc.ptyr_id = r.ptyr_id" + 
				"		  left outer join T_PTY_ACCOUNT ACC" + 
				"		  ON acc.account_id = racc.account_id" + 
				"	LEFT JOIN T_CTS_AGREEMENT_INFO AGRE" + 
				"	ON AGRE.COD_AGREEMENT =  AD.AGREEMENT_CODE" + 
				"	WHERE "
				.concat(startDate != null && endDate != null ? " ad.insert_timestamp between '"+startDate+"' and '"+endDate+"'" 
						: "	to_char(AD.insert_timestamp,'mm') = to_char(sysdate,'mm') and to_char(AD.insert_timestamp,'yyyy') = to_char(sysdate,'yyyy')") +  
				"	AND AD.RETURN_CODE IN ('1','01')" + 
				"	order by AD.insert_timestamp desc, AD.BANK_CODE, AD.SEQUENCIAL_NUMBER, AD.DEBIT_DATE, F.POLICY_NO , L.FILE_NAME";
	}
	
	public static String getIOF(String startDate, String endDate) {
		return 	"  SELECT " + 
				"  DECODE(pol.product_id, 1090003080, 'PATIT', 'New_Biz') as Type_Query," + 
				"  (SELECT PRDT.MAIN_SUSEP_LOB" + 
				"      FROM T_CTS_PRDT PRDT" + 
				"     WHERE PRDT.PRODUCT_ID = POL.PRODUCT_ID) AS \"LOB\"," + 
				"  POL.POLICY_NO AS \"Policy_No\"," + 
				"  NULL AS \"Endorsement_or_Declaration_no\", " + 
				"  (SELECT CURRENCY_NAME FROM T_CURRENCY WHERE CURRENCY_ID = FD.CURRENCY_ID) AS \"Currency\"," + 
				"  FD.ARAP_REF_NO AS \"Debit_Note_Installment\"," + 
				"  BC.RECEIPT_DATE," + 
				"  BC.UPDATE_TIME AS TRANSACTION_DATE," + 
				"  case" + 
				"     when (SELECT count (distinct related_pcode)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) > 1" + 
				"       then" + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       " + 
				"       " + 
				"       and related_pcode in (select related_pcode from T_BCP_FEE_DETAIL T2 where fd.arap_ref_no = t2.arap_ref_no)" + 
				"       AND T.FEE_TYPE IN (1001706))" + 
				"       else" + 
				"         (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706))" + 
				"       end AS \"Premium\"," + 
				"  case" + 
				"    when" + 
				"      (SELECT count (distinct related_pcode)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) > 1" + 
				"    then" + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       " + 
				"       and t.related_pcode in (select related_pcode from T_BCP_FEE_DETAIL T2 where fd.arap_ref_no = t2.arap_ref_no)" + 
				"       " + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     else" + 
				"       (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     end AS \"IOF\"," + 
				"  BC.RECEIPT_NO," + 
				"  CASE" + 
				"     WHEN BC.OPERATOR_ID in ('-11','14040') THEN" + 
				"      'Baixa Automática'" + 
				"     ELSE" + 
				"      'Baixa Manual'" + 
				"  END TIPO_BAIXA," + 
				"  BCPM.PAY_MODE_NAME," + 
				"  BC.DIRECT_ER," + 
				"  CP.MAJOR_LINE_CODE," + 
				"  CMIL.MINOR_LINE_CODE," + 
				"  CML.MAJOR_LINE_NAME," + 
				"  CASE" + 
				"     WHEN BC.PAY_MODE = 300 THEN" + 
				"      (SELECT SUBSTR(CB.BARCODE, 20, 7)" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS in (2,1))" + 
				"     ELSE" + 
				"      ''" + 
				"  END AS BARCODE," + 
				"  CASE" + 
				"     WHEN BC.PAY_MODE = 300 THEN" + 
				"      (SELECT NVL(CB.PER_IOF, 0.00)" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS in (2,1))" + 
				"     ELSE" + 
				"      0.00" + 
				"  END AS PER_IOF, " + 
				"  (SELECT max(B1.BOLETO_NO) FROM T_CTS_BOLETO B1" + 
				"  INNER JOIN T_CTS_BOLETO_SANT_RET_SEGT T1" + 
				"  on b1.boleto_id = t1.boleto_no" + 
				"  where b1.fee_id = fd.fee_id " + 
				"  and t1.movement_code in (6,17) " + 
				"  ) as BOLETO_CORRESPONDENTE," + 
				"  CBR.IOF_BANK_WITHHOLDING as FLAG_BAIXA" + 
				"    FROM T_POLICY_GENERAL_LOG POL," + 
				"         T_BCP_FEE          F," + 
				"         T_CTS_BCP_FEE      CF," + 
				"         T_BCP_FEE_DETAIL   FD," + 
				"         T_BCP_RELATION     BR," + 
				"         T_CTS_BCP_RELATION     CBR," + 
				"         T_BCP_COLLECTION   BC," + 
				"         T_BCP_CFG_PAY_MODE BCPM," + 
				"         T_CTS_PRDT        CP," + 
				"         T_CTS_MAJOR_LINE  CML," + 
				"         T_GEN_POLICY_INFO GPI," + 
				"         T_CTS_MINOR_LINE  CMIL" + 
				"  WHERE POL.ENDO_ID = POL.FIRST_ENDO_ID" + 
				"       AND POL.BACKUP_FLAG = 1" + 
				"       AND POL.POLICY_CATE = 1" + 
				"       AND POL.STATUS_ID = 120" + 
				"       AND (POL.MP_CONTRACT_ID IS NULL or pol.product_id = 1090003080)" + 
				"       AND CBR.relation_id = BR.relation_id" + 
				"		AND ( (UPPER(BCPM.PAY_MODE_NAME) = 'BOLETO' AND CBR.IOF_BANK_WITHHOLDING = 0) OR UPPER(BCPM.PAY_MODE_NAME) != 'BOLETO')" +
				"  and BC.PAY_MODE = BCPM.PAY_MODE_ID" + 
				"     AND F.TRANS_ID = CF.TRANS_ID" + 
				"     AND FD.TRANS_ID = F.TRANS_ID" + 
				"     AND F.POLICY_ID = POL.POLICY_ID" + 
				"     AND F.ENDO_ID IS NULL" + 
				"     AND FD.FEE_ID = BR.DEBIT_ID" + 
				"     AND BR.COLLECTION_ID = BC.COLLECTION_ID" + 
				"     AND POL.PRODUCT_ID = CP.PRODUCT_ID" + 
				"     AND POL.POLICY_ID = GPI.POLICY_ID" + 
				"     AND CP.MAJOR_LINE_CODE = CML.MAJOR_LINE_CODE" + 
				"     AND GPI.FIELD05 = CMIL.ID(+)" + 
				"     AND F.MODULE_ID IN (1001, 1002, 1003)" + 
				"     AND FD.FEE_TYPE = 1001101" + 
				"     AND FD.ARAP_STATUS IN (1, 5)" + 
				"     AND BC.UPDATE_TIME >= TO_DATE('" + startDate+ "', 'DDMMYYYY HH24:MI:SS')" + 
				"     AND BC.UPDATE_TIME <= TO_DATE('" + endDate + "', 'DDMMYYYY HH24:MI:SS')" + 
				"    and not exists" + 
				"    (SELECT 1" + 
				"         from T_CTS_BOLETO_SANT_RET_SEGT cbcrst," + 
				"         T_CTS_BOLETO_SANT_RET_SEGU cbcrsu," + 
				"         t_cts_boleto               cb1" + 
				"   where" + 
				"   fd.fee_id = cb1.fee_id" + 
				"     and cbcrst.return_id = cbcrsu.return_id" + 
				"     and cbcrst.seg_seq = cbcrsu.seg_seq" + 
				"     and cb1.boleto_id = cbcrst.boleto_no" + 
				"     and cbcrst.movement_code in (6, 17)" + 
				"     and cbcrsu.iof_value <> 0" + 
				"     )" + 
				"  and not exists (" + 
				"      SELECT 1" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          and bc.pay_mode <> 301" + 
				"          and fd.pay_mode = 300" + 
				"          AND CB.BOLETO_STATUS in (2,1)" + 
				"          and SUBSTR(CB.BARCODE, 20, 7) in ('4061527','4062728')" + 
				"          )" + 
				"  and not exists (" + 
				"      SELECT 1" + 
				"         FROM t_bcp_cash_fee_detail bcfd, t_bcp_relation br, t_bcp_fee_detail bfd, T_CTS_BOLETO CB" + 
				"        WHERE bcfd.receipt_no = bc.receipt_no" + 
				"          and CB.FEE_ID = FD.FEE_ID" + 
				"          and bcfd.fee_type = 9101002" + 
				"          and bc.collection_id = br.collection_id" + 
				"          and br.debit_id = bfd.fee_id" + 
				"          and BC.OPERATOR_ID <> -11" + 
				"          and BFD.PAY_MODE = 300" + 
				"          and bc.pay_mode <> 301" + 
				"          and SUBSTR(CB.BARCODE, 20, 7) in ('4061527','4062728')" + 
				"          )" + 
				"  and nvl(case" + 
				"    when" + 
				"      (SELECT count (distinct related_pcode)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) > 1" + 
				"    then" + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       " + 
				"       and t.related_pcode in (select related_pcode from T_BCP_FEE_DETAIL T2 where fd.arap_ref_no = t2.arap_ref_no)" + 
				"       " + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     else" + 
				"       (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = t.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = t.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     end,0) <> 0" + 
				"  UNION ALL" + 
				"  SELECT " + 
				"  'Endorsement' as Type_Query," + 
				"  (SELECT PRDT.MAIN_SUSEP_LOB" + 
				"      FROM T_CTS_PRDT PRDT" + 
				"     WHERE PRDT.PRODUCT_ID = POL.PRODUCT_ID) AS \"LOB\"," + 
				"  POL.POLICY_NO AS \"Policy_No\"," + 
				"  ENDO.ENDO_CODE AS \"Endorsement_or_Declaration_no\"," + 
				"  (SELECT CURRENCY_NAME FROM T_CURRENCY WHERE CURRENCY_ID = FD.CURRENCY_ID) AS \"Currency\"," + 
				"  FD.ARAP_REF_NO AS \"Debit_Note_Installment\"," + 
				"  BC.RECEIPT_DATE," + 
				"  BC.UPDATE_TIME AS TRANSACTION_DATE," + 
				"  case" + 
				"     when (SELECT count (distinct related_pcode)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) > 1" + 
				"       then" + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       " + 
				"       and related_pcode in (select related_pcode from T_BCP_FEE_DETAIL T2 where fd.arap_ref_no = t2.arap_ref_no)" + 
				"       " + 
				"       AND T.FEE_TYPE IN (1001706))" + 
				"       else" + 
				"         (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706))" + 
				"       end AS \"Premium\"," + 
				"  case" + 
				"    when" + 
				"      (SELECT count (distinct related_pcode)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) > 1" + 
				"    then" + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       " + 
				"       and t.related_pcode in (select related_pcode from T_BCP_FEE_DETAIL T2 where fd.arap_ref_no = t2.arap_ref_no)" + 
				"       " + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     else" + 
				"       (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     end AS \"IOF\"," + 
				"  BC.RECEIPT_NO," + 
				"  CASE" + 
				"     WHEN BC.OPERATOR_ID in ('-11','14040') THEN" + 
				"      'Baixa Automática'" + 
				"     ELSE" + 
				"      'Baixa Manual'" + 
				"  END TIPO_BAIXA," + 
				"  BCPM.PAY_MODE_NAME," + 
				"  BC.DIRECT_ER," + 
				"  CP.MAJOR_LINE_CODE," + 
				"  CMIL.MINOR_LINE_CODE," + 
				"  CML.MAJOR_LINE_NAME," + 
				"  CASE" + 
				"     WHEN BC.PAY_MODE = 300 THEN" + 
				"      (SELECT SUBSTR(CB.BARCODE, 20, 7)" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS = 2)" + 
				"     ELSE" + 
				"      ''" + 
				"  END AS BARCODE," + 
				"  CASE" + 
				"     WHEN BC.PAY_MODE = 300 THEN" + 
				"      (SELECT NVL(CB.PER_IOF, 0.00)" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS = 2)" + 
				"     ELSE" + 
				"      0.00" + 
				"  END AS PER_IOF, " + 
				"  (SELECT max(B1.BOLETO_NO) FROM T_CTS_BOLETO B1" + 
				"  INNER JOIN T_CTS_BOLETO_SANT_RET_SEGT T1" + 
				"  on b1.boleto_id = t1.boleto_no" + 
				"  where b1.fee_id = fd.fee_id " + 
				"  and t1.movement_code in (6,17) " + 
				"  ) as BOLETO_CORRESPONDENTE," + 
				"  CBR.IOF_BANK_WITHHOLDING as FLAG_BAIXA" + 
				"    FROM T_GEDO_ENDORSEMENT ENDO," + 
				"         T_POLICY_GENERAL_LOG POL," + 
				"         T_BCP_FEE          F," + 
				"         T_CTS_BCP_FEE      CF," + 
				"         T_BCP_FEE_DETAIL   FD," + 
				"         T_BCP_RELATION     BR," + 
				"         T_CTS_BCP_RELATION     CBR," + 
				"         T_BCP_COLLECTION   BC," + 
				"         T_BCP_CFG_PAY_MODE BCPM," + 
				"         T_CTS_PRDT        CP," + 
				"         T_CTS_MAJOR_LINE  CML," + 
				"         T_GEN_POLICY_INFO GPI," + 
				"         T_CTS_MINOR_LINE  CMIL" + 
				"  WHERE POL.ENDO_ID = ENDO.NEXT_ENDO_ID" + 
				"       AND POL.BACKUP_FLAG = 1" + 
				"       AND ENDO.STATUS_ID IN (300, 400)" + 
				"       AND POL.POLICY_CATE = 1" + 
				"       AND CBR.relation_id = BR.relation_id" + 
				"		AND ( (UPPER(BCPM.PAY_MODE_NAME) = 'BOLETO' AND CBR.IOF_BANK_WITHHOLDING = 0) OR UPPER(BCPM.PAY_MODE_NAME) != 'BOLETO')" +
				"  and BC.PAY_MODE = BCPM.PAY_MODE_ID" + 
				"     AND F.TRANS_ID = CF.TRANS_ID" + 
				"     AND FD.TRANS_ID = F.TRANS_ID" + 
				"     AND F.POLICY_ID = POL.POLICY_ID" + 
				"     AND F.ENDO_ID = ENDO.ENDO_ID" + 
				"     AND FD.FEE_ID = BR.DEBIT_ID" + 
				"     AND BR.COLLECTION_ID = BC.COLLECTION_ID" + 
				"     AND POL.PRODUCT_ID = CP.PRODUCT_ID" + 
				"     AND POL.POLICY_ID = GPI.POLICY_ID" + 
				"     AND CP.MAJOR_LINE_CODE = CML.MAJOR_LINE_CODE" + 
				"     AND GPI.FIELD05 = CMIL.ID(+)" + 
				"     AND F.MODULE_ID IN (1001, 1002, 1003)" + 
				"     AND FD.FEE_TYPE = 1001101" + 
				"     AND FD.ARAP_STATUS IN (1, 5)" + 
				"     AND BC.UPDATE_TIME >= TO_DATE('" + startDate+ "', 'DDMMYYYY HH24:MI:SS')" + 
				"     AND BC.UPDATE_TIME <= TO_DATE('" + endDate + "', 'DDMMYYYY HH24:MI:SS')" + 
				"     " + 
				"    and not exists" + 
				"    (SELECT 1" + 
				"         from T_CTS_BOLETO_SANT_RET_SEGT cbcrst," + 
				"         T_CTS_BOLETO_SANT_RET_SEGU cbcrsu," + 
				"         t_cts_boleto               cb1" + 
				"   where" + 
				"   fd.fee_id = cb1.fee_id" + 
				"     and cbcrst.return_id = cbcrsu.return_id" + 
				"     and cbcrst.seg_seq = cbcrsu.seg_seq" + 
				"     and cb1.boleto_id = cbcrst.boleto_no" + 
				"     and cbcrst.movement_code in (6, 17)" + 
				"     and cbcrsu.iof_value <> 0" + 
				"     )" + 
				"  and not exists (" + 
				"      SELECT 1" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          and bc.pay_mode <> 301" + 
				"          and fd.pay_mode = 300" + 
				"          AND CB.BOLETO_STATUS in (2,1)" + 
				"          and SUBSTR(CB.BARCODE, 20, 7) in ('4061527','4062728')" + 
				"          )" + 
				"  and not exists (" + 
				"      SELECT 1" + 
				"         FROM T_CTS_BCP_HSBC_BOLETO_RETURN cbhbr,T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          and cbhbr.document_code_number1 = CB.boleto_no" + 
				"          and SUBSTR(CB.BARCODE, 20, 7) in ('4061527','4062728')" + 
				"          ) " + 
				"  and not exists (" + 
				"      SELECT 1" + 
				"         FROM t_bcp_cash_fee_detail bcfd, t_bcp_relation br, t_bcp_fee_detail bfd, T_CTS_BOLETO CB" + 
				"        WHERE bcfd.receipt_no = bc.receipt_no" + 
				"          and CB.FEE_ID = FD.FEE_ID" + 
				"          and bcfd.fee_type = 9101002" + 
				"          and bc.collection_id = br.collection_id" + 
				"          and br.debit_id = bfd.fee_id" + 
				"          and BC.OPERATOR_ID <> -11" + 
				"          and BFD.PAY_MODE = 300" + 
				"          and bc.pay_mode <> 301" + 
				"          and SUBSTR(CB.BARCODE, 20, 7) in ('4061527','4062728')" + 
				"          )" + 
				"  and nvl(case" + 
				"    when" + 
				"      (SELECT count (distinct related_pcode)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) > 1" + 
				"    then" + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       " + 
				"       and t.related_pcode  in (select related_pcode from T_BCP_FEE_DETAIL T2 where fd.arap_ref_no = t2.arap_ref_no)" + 
				"       " + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     else" + 
				"       (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = t.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = t.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     end,0) <> 0" + 
				"  Union all" + 
				"  SELECT " + 
				"  'Declaration' as Type_Query," + 
				"  (SELECT PRDT.MAIN_SUSEP_LOB" + 
				"      FROM T_CTS_PRDT PRDT" + 
				"     WHERE PRDT.PRODUCT_ID = POL.PRODUCT_ID) AS \"LOB\"," + 
				"  POL.POLICY_NO AS \"Policy_No\"," + 
				"  DECL.DECLARATION_NO AS \"Endorsement_or_Declaration_no\"," + 

				"  (SELECT CURRENCY_NAME FROM T_CURRENCY WHERE CURRENCY_ID = FD.CURRENCY_ID) AS \"Currency\"," + 
				"  FD.ARAP_REF_NO AS \"Debit_Note_Installment\"," + 
				"  BC.RECEIPT_DATE," + 
				"  BC.UPDATE_TIME AS TRANSACTION_DATE," + 
				"  case" + 
				"     when (SELECT count (distinct related_pcode)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) > 1" + 
				"       then" + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       " + 
				"       and related_pcode in (select related_pcode from T_BCP_FEE_DETAIL T2 where fd.arap_ref_no = t2.arap_ref_no)" + 
				"       " + 
				"       AND T.FEE_TYPE IN (1001706))" + 
				"       else" + 
				"         (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706))" + 
				"       end AS \"Premium\"," + 
				"  case" + 
				"    when" + 
				"      (SELECT count (distinct related_pcode)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) > 1" + 
				"    then" + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       " + 
				"       and t.related_pcode in (select related_pcode from T_BCP_FEE_DETAIL T2 where fd.arap_ref_no = t2.arap_ref_no)" + 
				"       " + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     else" + 
				"       (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     end AS \"IOF\"," + 
				"  BC.RECEIPT_NO," + 
				"  CASE" + 
				"     WHEN BC.OPERATOR_ID in ('-11','14040') THEN" + 
				"      'Baixa Automática'" + 
				"     ELSE" + 
				"      'Baixa Manual'" + 
				"  END TIPO_BAIXA," + 
				"  BCPM.PAY_MODE_NAME," + 
				"  BC.DIRECT_ER," + 
				"  CP.MAJOR_LINE_CODE," + 
				"  CMIL.MINOR_LINE_CODE," + 
				"  CML.MAJOR_LINE_NAME," + 
				"  CASE" + 
				"     WHEN BC.PAY_MODE = 300 THEN" + 
				"      (SELECT SUBSTR(CB.BARCODE, 20, 7)" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS = 2)" + 
				"     ELSE" + 
				"      ''" + 
				"  END AS BARCODE," + 
				"  CASE" + 
				"     WHEN BC.PAY_MODE = 300 THEN" + 
				"      (SELECT NVL(CB.PER_IOF, 0.00)" + 
				"         FROM T_CTS_BOLETO CB" + 

				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS = 2)" + 
				"     ELSE" + 
				"      0.00" + 
				"  END AS PER_IOF, " + 
				"  (SELECT max(B1.BOLETO_NO) FROM T_CTS_BOLETO B1" + 
				"  INNER JOIN T_CTS_BOLETO_SANT_RET_SEGT T1" + 
				"  on b1.boleto_id = t1.boleto_no" + 
				"  where b1.fee_id = fd.fee_id " + 
				"  and t1.movement_code in (6,17) " + 
				"  ) as BOLETO_CORRESPONDENTE," + 
				"  CBR.IOF_BANK_WITHHOLDING as FLAG_BAIXA" + 
				"    FROM T_MP_DECLARATION     DECL," + 
				"         T_CTS_MP_DECLARATION CDECL," + 
				"         T_MP_CONTRACT        MC," + 
				"         T_POLICY_GENERAL     POL," + 
				"         T_BCP_FEE          F," + 
				"         T_CTS_BCP_FEE      CF," + 
				"         T_BCP_FEE_DETAIL   FD," + 
				"         T_BCP_RELATION     BR," + 
				"         T_CTS_BCP_RELATION     CBR," + 
				"         T_BCP_COLLECTION   BC," + 
				"         T_BCP_CFG_PAY_MODE BCPM," + 
				"         T_CTS_PRDT        CP," + 
				"         T_CTS_MAJOR_LINE  CML," + 
				"         T_GEN_POLICY_INFO GPI," + 
				"         T_CTS_MINOR_LINE  CMIL" + 
				"  WHERE DECL.DECL_ID = CDECL.DECL_ID" + 
				"       AND DECL.MASTER_POLICY_ID = MC.MP_CONTRACT_ID" + 
				"       AND MC.MP_CONTRACT_CODE = POL.POLICY_NO" + 
				"       AND NVL(DECL.DECLARATION_NO,'NULL') <> 'NULL'" + 
				"       AND POL.POLICY_CATE = 2" + 
				"       AND CBR.relation_id = BR.relation_id" + 
				"		AND ( (UPPER(BCPM.PAY_MODE_NAME) = 'BOLETO' AND CBR.IOF_BANK_WITHHOLDING = 0) OR UPPER(BCPM.PAY_MODE_NAME) != 'BOLETO')" +
				"  and BC.PAY_MODE = BCPM.PAY_MODE_ID" + 
				"     AND F.TRANS_ID = CF.TRANS_ID" + 
				"     AND FD.TRANS_ID = F.TRANS_ID" + 
				"     AND CF.DECL_NO = DECL.DECLARATION_NO" + 
				"     AND F.POLICY_CATEGORY = 3" + 
				"     AND FD.FEE_ID = BR.DEBIT_ID" + 
				"     AND BR.COLLECTION_ID = BC.COLLECTION_ID" + 
				"     AND POL.PRODUCT_ID = CP.PRODUCT_ID" + 
				"     AND POL.POLICY_ID = GPI.POLICY_ID" + 
				"     AND CP.MAJOR_LINE_CODE = CML.MAJOR_LINE_CODE" + 
				"     AND GPI.FIELD05 = CMIL.ID(+)" + 
				"     AND F.MODULE_ID IN (1001, 1002, 1003)" + 
				"     AND FD.FEE_TYPE = 1001101" + 
				"     AND FD.ARAP_STATUS IN (1, 5)" + 
				"     AND BC.UPDATE_TIME >= TO_DATE('" + startDate+ "', 'DDMMYYYY HH24:MI:SS')" + 
				"     AND BC.UPDATE_TIME <= TO_DATE('" + endDate + "', 'DDMMYYYY HH24:MI:SS')" + 
				"     " + 
				"     " + 
				"    and not exists" + 
				"    (SELECT 1" + 
				"         from T_CTS_BOLETO_SANT_RET_SEGT cbcrst," + 
				"         T_CTS_BOLETO_SANT_RET_SEGU cbcrsu," + 
				"         t_cts_boleto               cb1" + 
				"   where" + 
				"   fd.fee_id = cb1.fee_id" + 
				"     and cbcrst.return_id = cbcrsu.return_id" + 
				"     and cbcrst.seg_seq = cbcrsu.seg_seq" + 
				"     and cb1.boleto_id = cbcrst.boleto_no" + 
				"     and cbcrst.movement_code in (6, 17)" + 
				"     and cbcrsu.iof_value <> 0" + 
				"     )" + 
				"  and not exists (" + 
				"      SELECT 1" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          and bc.pay_mode <> 301" + 
				"          and fd.pay_mode = 300" + 
				"          AND CB.BOLETO_STATUS in (2,1)" + 
				"          and SUBSTR(CB.BARCODE, 20, 7) in ('4061527','4062728')" + 
				"          )" + 
				"  and not exists (" + 
				"      SELECT 1" + 
				"         FROM T_CTS_BCP_HSBC_BOLETO_RETURN cbhbr,T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = FD.FEE_ID" + 
				"          and cbhbr.document_code_number1 = CB.boleto_no" + 
				"          and SUBSTR(CB.BARCODE, 20, 7) in ('4061527','4062728')" + 
				"          ) " + 
				"  and not exists (" + 
				"      SELECT 1" + 
				"         FROM t_bcp_cash_fee_detail bcfd, t_bcp_relation br, t_bcp_fee_detail bfd, T_CTS_BOLETO CB" + 
				"        WHERE bcfd.receipt_no = bc.receipt_no" + 
				"          and CB.FEE_ID = FD.FEE_ID" + 
				"          and bcfd.fee_type = 9101002" + 
				"          and bc.collection_id = br.collection_id" + 
				"          and br.debit_id = bfd.fee_id" + 
				"          and BC.OPERATOR_ID <> -11" + 
				"          and BFD.PAY_MODE = 300" + 
				"          and bc.pay_mode <> 301" + 
				"          and SUBSTR(CB.BARCODE, 20, 7) in ('4061527','4062728')" + 
				"          )" + 
				"  and nvl(case" + 
				"    when" + 
				"      (SELECT count (distinct related_pcode)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = FD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) > 1" + 
				"    then" + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = FD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = FD.ARAP_STATUS" + 
				"       " + 
				"       and t.related_pcode in (select related_pcode from T_BCP_FEE_DETAIL T2 where fd.arap_ref_no = t2.arap_ref_no)" + 
				"       " + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     else" + 
				"       (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = F.TRANS_ID" + 
				"       AND T.CUR_PERIOD = t.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = t.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0)" + 
				"     end,0) <> 0" + 
				"  UNION ALL" + 
				"  SELECT" + 
				"  'Pre Pagamento' as Type_Query," + 

				"  (SELECT PRDT.MAIN_SUSEP_LOB" + 
				"      FROM T_CTS_PRDT PRDT" + 
				"     WHERE PRDT.PRODUCT_ID = PG.PRODUCT_ID) AS \"LOB\"," + 
				"  bf.POLICY_NO AS POLICY_NO," + 
				"  '' as Endorsement_or_Declaration_no," + 

				"  (SELECT CURRENCY_NAME FROM T_CURRENCY WHERE CURRENCY_ID = BFD.CURRENCY_ID) AS \"Currency\"," + 
				"  BFD.arap_ref_no as Debit_Note_Installment," + 
				"  bo.insert_time as RECEIPT_DATE," + 
				"  bo.update_time as TRANSACTION_DATE," + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = BFD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = BFD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = BFD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) AS \"Premium\"," + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = BF.TRANS_ID" + 
				"       AND T.CUR_PERIOD = BFD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = BFD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0) AS \"IOF\"," + 
				"  bo.offset_no as RECEIPT_NO," + 
				"  CASE" + 
				"     WHEN BO.OPERATOR_ID in ('-11','14040') THEN" + 
				"      'Baixa Automática'" + 
				"     ELSE" + 
				"      'Baixa Manual'" + 
				"  END TIPO_BAIXA," + 
				"  bcpm.pay_mode_name as PAY_MODE," + 
				"  bfd.direct_er as DIRECT_ER," + 
				"  CP.MAJOR_LINE_CODE," + 
				"  CMIL.MINOR_LINE_CODE," + 
				"  CML.MAJOR_LINE_NAME," + 
				"  CASE" + 
				"     WHEN BFD.PAY_MODE = 300 THEN" + 
				"      (SELECT SUBSTR(CB.BARCODE, 20, 7)" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = BFD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS in (2,1))" + 
				"     ELSE" + 
				"      ''" + 
				"  END AS BARCODE," + 
				"  CASE" + 
				"     WHEN BFD.PAY_MODE = 300 THEN" + 
				"      (SELECT NVL(CB.PER_IOF, 0.00)" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = BFD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS in (2,1))" + 
				"     ELSE" + 
				"      0.00" + 
				"  END AS PER_IOF, " + 
				"  (SELECT max(B1.BOLETO_NO) FROM T_CTS_BOLETO B1" + 
				"  INNER JOIN T_CTS_BOLETO_SANT_RET_SEGT T1" + 
				"  on b1.boleto_id = t1.boleto_no" + 
				"  where b1.fee_id = bfd.fee_id " + 
				"  and t1.movement_code in (6,17) " + 
				"  ) as BOLETO_CORRESPONDENTE," + 
				"  CBR.IOF_BANK_WITHHOLDING as FLAG_BAIXA" + 
				"    from T_bcp_offset     bo," + 
				"         T_bcp_relation   br," + 
				"         T_CTS_BCP_RELATION     CBR," + 
				"         T_bcp_fee_detail bfd," + 
				"         T_bcp_fee        bf," + 
				"         T_cts_boleto     cb," + 
				"         T_BCP_CFG_PAY_MODE BCPM," + 
				"         T_policy_general pg," + 
				"         T_CTS_PRDT         CP," + 
				"         T_CTS_MAJOR_LINE   CML," + 
				"         T_GEN_POLICY_INFO  GPI," + 
				"         T_CTS_MINOR_LINE   CMIL" + 
				"         " + 
				"  where bo.offset_id = br.offset_id" + 
				"     and br.debit_id = bfd.fee_id" + 
				"     and bfd.trans_id = bf.trans_id" + 
				"     and bf.policy_id = cb.policy_id" + 
				"     and bf.policy_id = pg.policy_id" + 
				"     and bfd.pay_mode = bcpm.pay_mode_id" + 
				"     AND CBR.relation_id = BR.relation_id" + 
				"		AND ( (UPPER(BCPM.PAY_MODE_NAME) = 'BOLETO' AND CBR.IOF_BANK_WITHHOLDING = 0) OR UPPER(BCPM.PAY_MODE_NAME) != 'BOLETO')" +
				"     " + 
				"     AND PG.PRODUCT_ID = CP.PRODUCT_ID" + 
				"     AND PG.POLICY_ID = GPI.POLICY_ID" + 
				"     AND CP.MAJOR_LINE_CODE = CML.MAJOR_LINE_CODE" + 
				"     AND GPI.FIELD05 = CMIL.ID(+)" + 
				"     " + 
				"     " + 
				"     and cb.prepayment = 'Y'" + 
				"     AND EXISTS (" + 
				"        SELECT 1" + 
				"        FROM T_BCP_FEE_DETAIL T" + 
				"        WHERE T.TRANS_ID = BF.TRANS_ID" + 
				"        AND T.CUR_PERIOD = BFD.CUR_PERIOD" + 
				"        AND T.ARAP_STATUS = BFD.ARAP_STATUS" + 
				"        AND T.FEE_TYPE IN (1001743, 1001745, 1001744)" + 
				"      HAVING SUM(T.AMOUNT) <> 0" + 
				"      " + 
				"     )" + 
				"     AND BO.UPDATE_TIME >= TO_DATE('" + startDate+ "', 'DDMMYYYY HH24:MI:SS')" + 
				"     AND BO.UPDATE_TIME <= TO_DATE('" + endDate + "', 'DDMMYYYY HH24:MI:SS')" + 
				"     and not exists (" + 
				"      SELECT 1" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = BFD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS in (2,1)" + 
				"          and SUBSTR(CB.BARCODE, 20, 7) in ('4061527','4062728')" + 
				"          )" + 
				"    and not exists" + 
				"    (SELECT 1" + 
				"         from T_CTS_BOLETO_SANT_RET_SEGT cbcrst," + 
				"         T_CTS_BOLETO_SANT_RET_SEGU cbcrsu," + 
				"         t_cts_boleto               cb1" + 
				"   where" + 
				"   bfd.fee_id = cb1.fee_id" + 
				"     and cbcrst.return_id = cbcrsu.return_id" + 
				"     and cbcrst.seg_seq = cbcrsu.seg_seq" + 
				"     and cb1.boleto_id = cbcrst.boleto_no" + 
				"     and cbcrst.movement_code in (6, 17)" + 
				"     and cbcrsu.iof_value <> 0" + 
				"     )" + 
				"     and CB.iof <> 0" + 
				"     and cb.boleto_status = 2" + 
				"     and bo.field01 is null  " + 
				"     and br.relation_type not in (7) " + 
				"  Union all" + 
				"  SELECT" + 
				"  'Total Loss' as Type_Query," + 
				"  (SELECT PRDT.MAIN_SUSEP_LOB" + 
				"      FROM T_CTS_PRDT PRDT" + 
				"     WHERE PRDT.PRODUCT_ID = PG.PRODUCT_ID) AS \"LOB\"," + 
				"  bf.POLICY_NO AS POLICY_NO," + 
				"  '' as Endorsement_or_Declaration_no," + 
				"  (SELECT CURRENCY_NAME FROM T_CURRENCY WHERE CURRENCY_ID = BFD.CURRENCY_ID) AS \"Currency\"," + 
				"  BFD.arap_ref_no as Debit_Note_Installment," + 
				"  bo.insert_time as RECEIPT_DATE," + 
				"  bo.update_time as TRANSACTION_DATE," + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = BFD.TRANS_ID" + 
				"       AND T.CUR_PERIOD = BFD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = BFD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001706)) AS \"Premium\"," + 
				"  (SELECT SUM(T.AMOUNT)" + 
				"      FROM T_BCP_FEE_DETAIL T" + 
				"     WHERE T.TRANS_ID = BF.TRANS_ID" + 
				"       AND T.CUR_PERIOD = BFD.CUR_PERIOD" + 
				"       AND T.ARAP_STATUS = BFD.ARAP_STATUS" + 
				"       AND T.FEE_TYPE IN (1001743, 1001745, 1001744) HAVING" + 
				"     SUM(T.AMOUNT) <> 0) AS \"IOF\"," + 
				"  bo.offset_no as RECEIPT_NO," + 
				"  CASE" + 
				"     WHEN BO.OPERATOR_ID in ('-11','14040') THEN" + 
				"      'Baixa Automática'" + 
				"     ELSE" + 
				"      'Baixa Manual'" + 
				"  END TIPO_BAIXA," + 
				"  bcpm.pay_mode_name as PAY_MODE," + 
				"  bfd.direct_er as DIRECT_ER," + 
				"  CP.MAJOR_LINE_CODE," + 
				"  CMIL.MINOR_LINE_CODE," + 
				"  CML.MAJOR_LINE_NAME," + 
				"  CASE" + 
				"     WHEN BFD.PAY_MODE = 300 THEN" + 
				"      (SELECT SUBSTR(CB.BARCODE, 20, 7)" + 
				"         FROM T_CTS_BOLETO CB" + 
				"        WHERE CB.FEE_ID = BFD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS in (2,1))" + 

				"     ELSE" + 
				"      ''" + 
				"  END AS BARCODE," + 
				"  CASE" + 

				"     WHEN BFD.PAY_MODE = 300 THEN" + 
				"      (SELECT NVL(CB.PER_IOF, 0.00)" + 
				"         FROM T_CTS_BOLETO CB" + 

				"        WHERE CB.FEE_ID = BFD.FEE_ID" + 
				"          AND CB.BOLETO_STATUS in (2,1))" + 

				"     ELSE" + 
				"      0.00" + 
				"  END AS PER_IOF, " + 
				"  (SELECT max(B1.BOLETO_NO) FROM T_CTS_BOLETO B1" + 
				"  INNER JOIN T_CTS_BOLETO_SANT_RET_SEGT T1" + 
				"  on b1.boleto_id = t1.boleto_no" + 
				"  where b1.fee_id = bfd.fee_id " + 
				"  and t1.movement_code in (6,17) " + 
				"  ) as BOLETO_CORRESPONDENTE," + 
				"  CBR.IOF_BANK_WITHHOLDING as FLAG_BAIXA" + 

				"    from T_bcp_offset     bo," + 
				"         T_bcp_relation   br," + 
				"         T_CTS_BCP_RELATION     CBR," + 
				"         T_bcp_fee_detail bfd," + 
				"         T_bcp_fee        bf," + 
				"         " + 
				"         T_BCP_CFG_PAY_MODE BCPM," + 
				"         T_policy_general pg," + 
				"         T_CTS_PRDT         CP," + 
				"         T_CTS_MAJOR_LINE   CML," + 
				"         T_GEN_POLICY_INFO  GPI," + 
				"         T_CTS_MINOR_LINE   CMIL" + 
				"         " + 
				"  where bo.offset_id = br.offset_id" + 
				"     and br.debit_id = bfd.fee_id" + 
				"     and bfd.trans_id = bf.trans_id" + 
				"     " + 
				"     and bf.policy_id = pg.policy_id" + 
				"     and bfd.pay_mode = bcpm.pay_mode_id" + 
				"     AND CBR.relation_id = BR.relation_id" + 
				"		AND ( (UPPER(BCPM.PAY_MODE_NAME) = 'BOLETO' AND CBR.IOF_BANK_WITHHOLDING = 0) OR UPPER(BCPM.PAY_MODE_NAME) != 'BOLETO')" +
				"     " + 
				"     AND PG.PRODUCT_ID = CP.PRODUCT_ID" + 
				"     AND PG.POLICY_ID = GPI.POLICY_ID" + 
				"     AND CP.MAJOR_LINE_CODE = CML.MAJOR_LINE_CODE" + 
				"     AND GPI.FIELD05 = CMIL.ID(+)" + 
				"     AND EXISTS (" + 
				"        SELECT 1" + 
				"        FROM T_BCP_FEE_DETAIL T" + 
				"        WHERE T.TRANS_ID = BF.TRANS_ID" + 
				"        AND T.CUR_PERIOD = BFD.CUR_PERIOD" + 
				"        AND T.ARAP_STATUS = BFD.ARAP_STATUS" + 
				"        AND T.FEE_TYPE IN (1001743, 1001745, 1001744)" + 
				"      HAVING SUM(T.AMOUNT) <> 0" + 
				"      " + 
				"     )" + 
				"     AND BO.UPDATE_TIME >= TO_DATE('" + startDate+ "', 'DDMMYYYY HH24:MI:SS')" + 
				"     AND BO.UPDATE_TIME <= TO_DATE('" + endDate + "', 'DDMMYYYY HH24:MI:SS')" + 
				"     and bo.field01 is null  " + 
				"     and br.relation_type in (7) " ;
	}
	
	public static String getDirect_BaixasManuais(String startDate, String endDate) {
		return "	SELECT " + 
				"		distinct DT.ARAP_REF_NO ," + 
				"		dt.cur_period , " + 
				"		dt.due_Date ," + 
				"		PAY.pay_mode_NAME," + 
				"		dt.amount, " + 
				"		dt.balance, " + 
				"		BCOL.receipt_no," + 
				"		bcol.insert_time as baixa_em, " + 
				"		bcol.receipt_date as recebimento_premio, " + 
				"		bcol.operator_id , " + 
				"		u.email, " + 
				"		T.FEE_NAME, " + 
				"		T.FEE_TYPE,  " + 
				"		F.POLICY_NO, " + 
				"		F.ENDO_NO," + 
				"		inf.FIELD06, " + 
				"		inv.sales_ter, " + 
				"		(CASE 	WHEN inv.sales_ter IN ('01','05','06') then 'A Account'" + 
				"				WHEN inv.sales_ter = '03'  then 'B Account'" + 
				"				WHEN  inv.sales_ter IN ('04','07') then 'C Account'" + 
				"				ELSE '' END) AS abc_account," + 
				"		g.product_code, " + 
				"		g.product_name, " + 
				"		(CASE 	WHEN BCOL.UPDATED_BY = '-11' THEN 'Baixa Automática' " + 
				"				when BCOL.UPDATED_BY is not null and BCOL.UPDATED_BY not like '-11' then 'Manual' " + 
				"				else '' end) as tipo_baixa" + 
				"		" + 
				"	FROM T_BCP_FEE_DETAIL DT" + 
				"		INNER JOIN T_BCP_FEE F" + 
				"		ON F.TRANS_ID = DT.TRANS_ID " + 
				"		inner join t_policy_general p " + 
				"		on p.policy_id = f.policy_id " + 
				"		inner join T_GEN_POLICY_INFO inf" + 
				"		on inf.policy_id = p.policy_id" + 
				"		inner join t_product_general g" + 
				"		on g.product_id = p.product_id " + 
				"		INNER JOIN T_BCP_CFG_FEE_TYPE T" + 
				"		ON T.FEE_TYPE = DT.FEE_TYPE " + 
				"		INNER JOIN  t_bcp_fee_detail dt2" + 
				"		ON dt2.rel_fee_id = dt.fee_id AND dt2.fee_type in (1053209,1003209,1073209)" + 
				"		inner join t_agt_agent agt" + 
				"		on agt.agent_code = dt2.related_pcode" + 
				"		" + 
				"		INNER JOIN t_bcp_cfg_pay_mode PAY" + 
				"		ON PAY.PAY_MODE_ID = DT.PAY_MODE" + 
				"		" + 
				"		inner join  T_CTS_BCP_COLLECTION_TRACE tra" + 
				"		on dt.fee_id = tra.debit_id " + 
				"		" + 
				"		inner join T_BCP_COLLECTION bcol" + 
				"		on tra.collection_id = bcol.collection_id" + 
				"		" + 
				"		inner join t_user u" + 
				"		on u.user_id = bcol.operator_id " + 
				"		left join T_INSURED_LIST lis" + 
				"		on lis.policy_id = p.policy_id" + 
				"		" + 
				"		left join T_POLICY_CT T" + 
				"		on T.INSURED_ID = lis.INSURED_ID         " + 
				"		" + 
				"		left JOIN T_CTS_PRDT_CT D " + 
				"		ON T.CT_ID = D.CT_ID" + 
				"	  " + 
				"		left join t_cts_major_line des " + 
				"		on des.major_line_code = 	D.MAJOR_LINE_CODE" + 
				"		left join T_CTS_BCP_LVLD_INVOICE inv" + 
				"		on inv.debit_note = dt.arap_Ref_no " + 
				"	where " + 
				"		BCOL.UPDATED_BY is not null " + 
				"		and BCOL.UPDATED_BY not like '-11' " + 
				"		and BCOL.UPDATED_BY not like '14040' " + 
				"		and bcol.insert_time >= TO_DATE('" + startDate +" 00:00:00', 'DDMMYYYY HH24:MI:SS') " + 
				"		and bcol.insert_time <= TO_DATE('" + endDate +" 23:59:59', 'DDMMYYYY HH24:MI:SS')" + 
				"		and D.MAJOR_LINE_CODE is not null " + 
				"		and D.MINOR_LINE_CODE is not null " + 
				"	  and ((p.policy_cate in (1) and p.mp_contract_id is null) or(p.policy_cate in (2) and p.mp_contract_id is not null) )" + 
				"	union all  " + 
				"	SELECT " + 
				"		distinct DT.ARAP_REF_NO," + 
				"		dt.cur_period as parcela, " + 
				"		dt.due_Date, " + 
				"		PAY.pay_mode_NAME," + 
				"		dt.amount, " + 
				"		dt.balance, " + 
				"		o.offset_no ,  " + 
				"		o.insert_time as baixa_em, " + 
				"		o.insert_time as recebimento_premio, " + 
				"		o.operator_id , " + 
				"		u.email, " + 
				"		T.FEE_NAME, " + 
				"		T.FEE_TYPE,  " + 
				"		F.POLICY_NO, " + 
				"		F.ENDO_NO," + 
				"		inf.FIELD06, " + 
				"		inv.sales_ter, " + 
				"		(CASE 	WHEN inv.sales_ter IN ('01','05','06') then 'A Account'" + 
				"				WHEN inv.sales_ter = '03' then 'B Account'" + 
				"				WHEN  inv.sales_ter IN ('04','07') then 'C Account'" + 
				"				ELSE '' END) AS abc_account," + 
				"		g.product_code, " + 
				"		g.product_name, " + 
				"		(CASE 	WHEN o.UPDATED_BY = '-11' THEN 'Baixa Automática' " + 
				"				when o.UPDATED_BY is not null and o.UPDATED_BY not like '-11' then 'Manual' " + 
				"				else '' end) as tipo_baixa" + 
				"		" + 
				"	FROM T_BCP_FEE_DETAIL DT" + 
				"		inner JOIN T_BCP_FEE F" + 
				"		ON F.TRANS_ID = DT.TRANS_ID " + 
				"		inner join t_policy_general p " + 
				"		on p.policy_id = f.policy_id " + 
				"		inner join T_GEN_POLICY_INFO inf" + 
				"		on inf.policy_id = p.policy_id" + 
				"		inner join t_product_general g" + 
				"		on g.product_id = p.product_id " + 
				"		inner join T_BCP_CFG_FEE_TYPE T" + 
				"		ON T.FEE_TYPE = DT.FEE_TYPE " + 
				"		inner join t_bcp_fee_detail dt2" + 
				"		ON dt2.rel_fee_id = dt.fee_id AND dt2.fee_type in (1053209,1003209,1073209)" + 
				"		" + 
				"		inner join t_agt_agent agt" + 
				"		on agt.agent_code = dt2.related_pcode" + 
				"		" + 
				"		inner join t_bcp_cfg_pay_mode PAY" + 
				"		ON PAY.PAY_MODE_ID = DT.PAY_MODE" + 
				"		" + 
				"		left join t_bcp_relation r" + 
				"		on r.debit_id = dt.fee_id" + 
				"		" + 
				"		left join t_bcp_offset o" + 
				"		on o.offset_id = r.offset_id" + 
				"		left join t_user u" + 
				"		on u.user_id = O.operator_id " + 
				"		left join T_INSURED_LIST lis" + 
				"		on lis.policy_id = p.policy_id" + 
				"		" + 
				"		left join T_POLICY_CT T" + 
				"		on T.INSURED_ID = lis.INSURED_ID         " + 
				"		" + 
				"		left JOIN T_CTS_PRDT_CT D " + 
				"		ON T.CT_ID = D.CT_ID" + 
				"	  left join t_cts_major_line des " + 
				"	  on des.major_line_code = 	D.MAJOR_LINE_CODE" + 
				"		left join T_CTS_BCP_LVLD_INVOICE inv" + 
				"		on inv.debit_note = dt.arap_Ref_no " +  
				"	where " + 
				"		O.OPERATOR_ID is not null " + 
				"		and O.OPERATOR_ID not like '-11' " + 
				"		and O.OPERATOR_ID not like '14040' " + 
				"		and O.insert_time >= TO_DATE('" + startDate +" 00:00:00', 'DDMMYYYY HH24:MI:SS') " + 
				"		and O.insert_time <= TO_DATE('" + endDate +" 23:59:59', 'DDMMYYYY HH24:MI:SS')" + 
				"		and D.MAJOR_LINE_CODE is not null " + 
				"		and D.MINOR_LINE_CODE is not null   " + 
				"	  and ((p.policy_cate in (1) and p.mp_contract_id is null) or(p.policy_cate in (2) and p.mp_contract_id is not null) )" + 
				"	  order by 8";
	}
	
	public static String getCoinsurance_BaixasManuais(String startDate, String endDate) {
		return 	" 	SELECT " + 
				"		distinct DT.ARAP_REF_NO," + 
				"		dt.cur_period as parcela, " + 
				"		dt.due_Date, " + 
				"		PAY.pay_mode_NAME," + 
				"		dt.amount, " + 
				"		dt.balance, " + 
				"		BCOL.receipt_no," + 
				"		bcol.insert_time as baixa_em, " + 
				"		bcol.receipt_date as recebimento_premio, " + 
				"		bcol.operator_id , " + 
				"		u.email, " + 
				"		T.FEE_NAME, " + 
				"		T.FEE_TYPE,  " + 
				"		F.POLICY_NO, " + 
				"		F.ENDO_NO," + 
				"		f.ri_policy_no, " + 
				"		inf.FIELD06, " + 
				"		inv.sales_ter, " + 
				"		(CASE 	WHEN inv.sales_ter IN ('01','05','06') then 'A Account'" + 
				"				WHEN inv.sales_ter = '03'  then 'B Account'" + 
				"				WHEN  inv.sales_ter IN ('04','07') then 'C Account'" + 
				"				ELSE '' END) AS abc_account," + 
				"		g.product_code, " + 
				"		g.product_name, " + 
				"		(CASE 	WHEN BCOL.UPDATED_BY = '-11' THEN 'Baixa Automática' " + 
				"				when BCOL.UPDATED_BY is not null and BCOL.UPDATED_BY not like '-11' then 'Manual' " + 
				"				else '' end) as tipo_baixa" + 
				"		" + 
				"	FROM T_BCP_FEE_DETAIL DT" + 
				"		INNER JOIN T_BCP_FEE F" + 
				"		ON F.TRANS_ID = DT.TRANS_ID " + 
				"	    " + 
				"		inner join t_policy_general p " + 
				"		on p.policy_id = f.policy_id " + 
				"		inner join T_GEN_POLICY_INFO inf" + 
				"		on inf.policy_id = p.policy_id" + 
				"		inner join t_product_general g" + 
				"		on g.product_id = p.product_id " + 
				"		INNER JOIN T_BCP_CFG_FEE_TYPE T" + 
				"		ON T.FEE_TYPE = DT.FEE_TYPE " + 
				"		left join t_bcp_fee f2" + 
				"		on f2.policy_id = f.policy_id " + 
				"		left join t_agt_agent agt" + 
				"		on agt.agent_code = f2.agent_pcode" + 
				"		" + 
				"		INNER JOIN t_bcp_cfg_pay_mode PAY" + 
				"		ON PAY.PAY_MODE_ID = DT.PAY_MODE" + 
				"		" + 
				"		inner join  T_CTS_BCP_COLLECTION_TRACE tra" + 
				"		on dt.fee_id = tra.debit_id " + 
				"		" + 
				"		inner join T_BCP_COLLECTION bcol" + 
				"		on tra.collection_id = bcol.collection_id" + 
				"		" + 
				"		inner join t_user u" + 
				"		on u.user_id = bcol.operator_id " + 
				"		left join T_INSURED_LIST lis" + 
				"		on lis.policy_id = p.policy_id" + 
				"		" + 
				"		left join T_POLICY_CT T" + 
				"		on T.INSURED_ID = lis.INSURED_ID         " + 
				"		" + 
				"		left JOIN T_CTS_PRDT_CT D " + 
				"		ON T.CT_ID = D.CT_ID" + 
				"		left join t_cts_major_line des " + 
				"		on des.major_line_code = 	D.MAJOR_LINE_CODE" + 
				"	  " + 
				"		left join T_CTS_BCP_LVLD_INVOICE inv" + 
				"		on inv.debit_note = dt.arap_Ref_no " + 
				"	where " + 
				"		BCOL.UPDATED_BY is not null " + 
				"		and BCOL.UPDATED_BY not like '-11' " + 
				"		and BCOL.UPDATED_BY not like '14040' " + 
				"		and bcol.insert_time >= TO_DATE('" + startDate + " 00:00:00', 'DDMMYYYY HH24:MI:SS')" + 
				"		and bcol.insert_time <= TO_DATE('" + endDate + " 23:59:59', 'DDMMYYYY HH24:MI:SS')" + 
				"		and D.MAJOR_LINE_CODE is not null " + 
				"		and D.MINOR_LINE_CODE is not null " + 
				"		and dt.fee_type <> 1001101" + 
				"	  and ((p.policy_cate in (1) and p.mp_contract_id is null) or(p.policy_cate in (2) and p.mp_contract_id is not null) )" + 
				"	  and agt.agent_code is not null" + 
				"	union all  " + 
				"	SELECT " + 
				"		distinct DT.ARAP_REF_NO," + 
				"		dt.cur_period as parcela, " + 
				"		dt.due_Date, " + 
				"		PAY.pay_mode_NAME," + 
				"		dt.amount, " + 
				"		dt.balance, " + 
				"		o.offset_no , " + 
				"		o.insert_time as baixa_em, " + 
				"		o.insert_time as recebimento_premio, " + 
				"		o.operator_id , " + 
				"		u.email, " + 
				"		T.FEE_NAME, " + 
				"		T.FEE_TYPE,  " + 
				"		F.POLICY_NO, " + 
				"		F.ENDO_NO," + 
				"	  f.ri_policy_no, " + 
				"		inf.FIELD06, " + 
				"		inv.sales_ter, " + 
				"		(CASE 	WHEN inv.sales_ter IN ('01','05','06') then 'A Account'" + 
				"				WHEN inv.sales_ter = '03' then 'B Account'" + 
				"				WHEN  inv.sales_ter IN ('04','07') then 'C Account'" + 
				"				ELSE '' END) AS abc_account," + 
				"		g.product_code, " + 
				"		g.product_name, " + 
				"		(CASE 	WHEN o.UPDATED_BY = '-11' THEN 'Baixa Automática' " + 
				"				when o.UPDATED_BY is not null and o.UPDATED_BY not like '-11' then 'Manual' " + 
				"				else '' end) as tipo_baixa" + 
				"	FROM T_BCP_FEE_DETAIL DT" + 
				"		inner JOIN T_BCP_FEE F" + 
				"		ON F.TRANS_ID = DT.TRANS_ID " + 
				"		inner join t_policy_general p " + 
				"		on p.policy_id = f.policy_id " + 
				"		inner join T_GEN_POLICY_INFO inf" + 
				"		on inf.policy_id = p.policy_id" + 
				"		inner join t_product_general g" + 
				"		on g.product_id = p.product_id " + 
				"		inner join T_BCP_CFG_FEE_TYPE T" + 
				"		ON T.FEE_TYPE = DT.FEE_TYPE " + 
				"		left join t_agt_agent agt" + 
				"		on agt.agent_code = f.agent_pcode" + 
				"		" + 
				"		inner join t_bcp_cfg_pay_mode PAY" + 
				"		ON PAY.PAY_MODE_ID = DT.PAY_MODE" + 
				"		" + 
				"		left join t_bcp_relation r" + 
				"		on r.debit_id = dt.fee_id" + 
				"		" + 
				"		left join t_bcp_offset o" + 
				"		on o.offset_id = r.offset_id" + 
				"		left join t_user u" + 
				"		on u.user_id = O.operator_id " + 
				"		left join T_INSURED_LIST lis" + 
				"		on lis.policy_id = p.policy_id" + 
				"		" + 
				"		left join T_POLICY_CT T" + 
				"		on T.INSURED_ID = lis.INSURED_ID         " + 
				"		" + 
				"		left JOIN T_CTS_PRDT_CT D " + 
				"		ON T.CT_ID = D.CT_ID" + 
				"		left join t_cts_major_line des " + 
				"		on des.major_line_code = 	D.MAJOR_LINE_CODE" + 
				"	  " + 
				"		left join T_CTS_BCP_LVLD_INVOICE inv" + 
				"		on inv.debit_note = dt.arap_Ref_no " + 
				"	where " + 
				"		O.OPERATOR_ID is not null " + 
				"		and O.OPERATOR_ID not like '-11' " + 
				"		and O.OPERATOR_ID not like '14040' " + 
				"		and O.insert_time >= TO_DATE('" + startDate + " 00:00:00', 'DDMMYYYY HH24:MI:SS')" + 
				"		and O.insert_time <= TO_DATE('" + endDate + " 23:59:59', 'DDMMYYYY HH24:MI:SS')" + 
				"		and D.MAJOR_LINE_CODE is not null " + 
				"		and D.MINOR_LINE_CODE is not null   " + 
				"		and dt.fee_type <> 1001101" + 
				"		and ((p.policy_cate in (1) and p.mp_contract_id is null) or(p.policy_cate in (2) and p.mp_contract_id is not null) )" + 
				"	    and agt.agent_code is not null" + 
				"	  order by 8";
	}

	public static String getCancelInadim(String startDate, String endDate) {
		return "  SELECT  " + 
				"    F.POLICY_NO," + 
				"    'REAL' AS MOEDA," + 
				"    DT.AMOUNT, " + 
				"    DT.CUR_PERIOD, " + 
				"    E.ISSUE_DATE " + 
				"  FROM T_POLICY_GENERAL P" + 
				"    inner join T_BCP_FEE F" + 
				"    ON F.POLICY_ID = P.POLICY_ID" + 
				"    INNER JOIN T_BCP_FEE_DETAIL DT" + 
				"    ON DT.TRANS_ID = F.TRANS_ID " + 
				"    INNER JOIN T_BCP_CFG_ARAP_STATUS S" + 
				"    ON S.STATUS_ID = DT.ARAP_sTATUS" + 
				"    INNER JOIN T_GEDO_ENDORSEMENT E" + 
				"    ON E.POLICY_ID = P.POLICY_ID" + 
				"  WHERE DT.ARAP_STATUS <> 6" + 
				"  AND S.STATUS_ID <> 1" + 
				"  AND DT.FEE_TYPE = 1001101" + 
				"  AND P.POLICY_NO IN (SELECT POLICY_NO" + 
				"                        FROM PRD1_ST.T_STG_POLICYBOOKING" + 
				"                        WHERE RECORD_TYPE_CD = 'X'" + 
				"                          AND REQUEST_FILE LIKE '%<cancelReasonId>22</cancelReasonId>%'" + 
				"                          AND PROCESS_STATUS = 'P'" + 
				"                          AND REQUEST_TIME BETWEEN TO_DATE('" + startDate +" 00:00:00', 'DDMMYYYY HH24:MI:SS') " +
				"                          AND TO_DATE('" + endDate +" 23:59:59', 'DDMMYYYY HH24:MI:SS')" + 
				"                      )" + 
				"  AND E.CANCEL_REASON_ID = 22" + 
				"  ORDER BY P.POLICY_NO,DT.CUR_PERIOD";
	}
}
