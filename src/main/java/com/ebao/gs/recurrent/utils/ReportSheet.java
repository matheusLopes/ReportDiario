package com.ebao.gs.recurrent.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReportSheet {
	public static void populateHeader(Row header, XSSFWorkbook new_workbook,String[] columns) {
		int count = 0;
		for(String column : columns) {
			header.createCell(count).setCellValue(column);
			count++;
		}
		XSSFCellStyle headerStyle = new_workbook.createCellStyle();
		XSSFFont bold = new_workbook.createFont();
		bold.setBold(true);
		bold.setFontHeight(10);
		headerStyle.setFont(bold);
		for(Cell cell : header) {
			cell.setCellStyle(headerStyle);
		}
	}
}
