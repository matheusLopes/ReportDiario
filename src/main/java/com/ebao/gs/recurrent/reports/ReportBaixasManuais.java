package com.ebao.gs.recurrent.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.gs.recurrent.jdbc.DatabaseConnection;
import com.ebao.gs.recurrent.jdbc.DatabaseInfo;
import com.ebao.gs.recurrent.utils.ReportSheet;
import com.ebao.gs.recurrent.utils.ReportsQueries;

public class ReportBaixasManuais {
	static Logger logger = LogManager.getLogger(ReportBaixasManuais.class.getName());

	public static void generateReport() throws SQLException, IOException, ClassNotFoundException, GeneralSecurityException {
		String session = DatabaseInfo.getSession();
		String path = DatabaseInfo.getOutputDir();
		
		DatabaseConnection db = new DatabaseConnection();
		Connection conn = db.connect(DatabaseInfo.getDatabase(), DatabaseInfo.getUser(), DatabaseInfo.getPass());
		Statement stmt = conn.createStatement();
		
		/* Create Workbook and Worksheet objects */
		XSSFWorkbook new_workbook = new XSSFWorkbook();
		// create a blank workbook object
		XSSFSheet sheet = new_workbook.createSheet("Direto"); // create a worksheet with caption score_details
		/* Create a header to the sheet*/
		Row header = sheet.createRow(0);
		
		ReportSheet.populateHeader(header, new_workbook,
				new String[] { "", "ARAP_REF_NO", "CUR_PERIOD", "DUE_DATE", "PAY_MODE_NAME", "AMOUNT", "BALANCE",
						"RECEIPT_NO", "BAIXA_EM", "RECEBIMENTO_PREMIO", "OPERATOR_ID", "EMAIL", "FEE_NAME", "FEE_TYPE",
						"POLICY_NO", "ENDO_NO", "FIELD06",
						"SALES_TER", "ABC_ACCOUNT", "PRODUCT_CODE", "PRODUCT_NAME", "TIPO_BAIXA" });
		
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		Calendar c1 = Calendar.getInstance();
		String startDate = null;
		String endDate = null;
		
		int day = c1.get(Calendar.DAY_OF_MONTH);
		c1.add(Calendar.DATE, -day);
		endDate = sdf.format(c1.getTime());
		c1.set(Calendar.DAY_OF_MONTH, 1);
		startDate = sdf.format(c1.getTime());		
		
		logger.debug("Startdate: " + startDate + ", Enddate:" + endDate);

		logger.debug("Query started.");
		stmt.executeQuery("ALTER SESSION SET CURRENT_SCHEMA = " + session);
		ResultSet query_set = stmt.executeQuery(ReportsQueries.getDirect_BaixasManuais(startDate, endDate));
		/* Create Map for Excel Data */
		Map<String, Object[]> excel_data = new HashMap<String, Object[]>(); // create a map and define data
		int row_counter = 1;
		/* Populate data into the Map */
		while (query_set.next()) {
			row_counter = row_counter + 1;
			String a 	= query_set.getString("ARAP_REF_NO");
			Long b 		= query_set.getLong("CUR_PERIOD");
			Date c 		= query_set.getDate("DUE_DATE");
			String d 	= query_set.getString("PAY_MODE_NAME");
			double e 	= query_set.getDouble("AMOUNT");
			double f 	= query_set.getDouble("BALANCE");
			String g 	= query_set.getString("RECEIPT_NO");
			Timestamp h = query_set.getTimestamp("BAIXA_EM");
			Date i 		= query_set.getDate("RECEBIMENTO_PREMIO");
			Long j 		= query_set.getLong("OPERATOR_ID");
			String k 	= query_set.getString("EMAIL");
			String l 	= query_set.getString("FEE_NAME");
			Long m 		= query_set.getLong("FEE_TYPE");
			String n 	= query_set.getString("POLICY_NO");
			String o 	= query_set.getString("ENDO_NO");
			String p 	= query_set.getString("FIELD06");
			String q 	= query_set.getString("SALES_TER");
			String r 	= query_set.getString("ABC_ACCOUNT");
			String s 	= query_set.getString("PRODUCT_CODE");
			String t 	= query_set.getString("PRODUCT_NAME");
			String u 	= query_set.getString("TIPO_BAIXA");
			excel_data.put(Integer.toString(row_counter),
					new Object[] { a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u });
		}
		/* Close all DB related objects */
		query_set.close();

		CellStyle dateStyle = new_workbook.createCellStyle();
		CreationHelper createHelper = new_workbook.getCreationHelper();
		short dateFormat = createHelper.createDataFormat().getFormat("dd/MM/yyyy");
		dateStyle.setDataFormat(dateFormat);

		CellStyle dateTimeStyle = new_workbook.createCellStyle();
		CreationHelper createTimeHelper = new_workbook.getCreationHelper();
		short dateTimeFormat = createTimeHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss");
		dateTimeStyle.setDataFormat(dateTimeFormat);
		DataFormat format = new_workbook.createDataFormat();
		CellStyle doubleStyle = new_workbook.createCellStyle();
		doubleStyle.setDataFormat(format.getFormat("0.00"));
		/* Load data into logical worksheet */
		Set<String> keyset = excel_data.keySet();
		int rownum = 1;
		for (String key : keyset) { // loop through the data and add them to the cell
			Row row = sheet.createRow(rownum++);
			Object[] objArr = excel_data.get(key);
			Cell id = row.createCell(0);
			id.setCellValue(rownum - 1);
			int cellnum = 1;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof Long) {
					cell.setCellValue((Long) obj);
				} else if (obj instanceof Double) {
					cell.setCellStyle(doubleStyle);
					cell.setCellValue((Double) obj);
				} else if (obj instanceof Timestamp) {
					cell.setCellStyle(dateTimeStyle);
					cell.setCellValue((Timestamp) obj);
				} else if (obj instanceof Date) {
					cell.setCellStyle(dateStyle);
					cell.setCellValue((Date) obj);
				} else
					cell.setCellValue((String) obj);
			}
		}
		
		XSSFSheet sheetCos = new_workbook.createSheet("Cosseguro");
		Row headerCos = sheetCos.createRow(0);
				
		ReportSheet.populateHeader(headerCos, new_workbook,
				new String[] { "", "ARAP_REF_NO", "PARCELA", "DUE_DATE", "PAY_MODE_NAME", "AMOUNT", "BALANCE",
						"RECEIPT_NO", "BAIXA_EM", "RECEBIMENTO_PREMIO", "OPERATOR_ID", "EMAIL", "FEE_NAME", "FEE_TYPE",
						"POLICY_NO", "ENDO_NO", "RI_POLICY_NO", "FIELD06", "SALES_TER", "ABC_ACCOUNT", "PRODUCT_CODE", 
						"PRODUCT_NAME", "TIPO_BAIXA" });
		
		logger.debug("Query started.");
		ResultSet query_setCos = stmt.executeQuery(ReportsQueries.getCoinsurance_BaixasManuais(startDate, endDate));
		/* Create Map for Excel Data */
		excel_data = new HashMap<String, Object[]>();
		
		row_counter = 1;
		/* Populate data into the Map */
		while (query_setCos.next()) {
			row_counter = row_counter + 1;
			String a 	= query_setCos.getString("ARAP_REF_NO");
			Long b 		= query_setCos.getLong("PARCELA");
			Date c 		= query_setCos.getDate("DUE_DATE");
			String d 	= query_setCos.getString("PAY_MODE_NAME");
			double e 	= query_setCos.getDouble("AMOUNT");
			double f 	= query_setCos.getDouble("BALANCE");
			String g 	= query_setCos.getString("RECEIPT_NO");
			Timestamp h = query_setCos.getTimestamp("BAIXA_EM");
			Date i 		= query_setCos.getDate("RECEBIMENTO_PREMIO");
			Long j 		= query_setCos.getLong("OPERATOR_ID");
			String k 	= query_setCos.getString("EMAIL");
			String l 	= query_setCos.getString("FEE_NAME");
			Long m 		= query_setCos.getLong("FEE_TYPE");
			String n 	= query_setCos.getString("POLICY_NO");
			String o 	= query_setCos.getString("ENDO_NO");
			String p 	= query_setCos.getString("RI_POLICY_NO");
			String q 	= query_setCos.getString("FIELD06");
			String r 	= query_setCos.getString("SALES_TER");
			String s 	= query_setCos.getString("ABC_ACCOUNT");
			String t 	= query_setCos.getString("PRODUCT_CODE");
			String u 	= query_setCos.getString("PRODUCT_NAME");
			String v 	= query_setCos.getString("TIPO_BAIXA");
			excel_data.put(Integer.toString(row_counter),
					new Object[] { a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v });
		}
		query_setCos.close();
		
		Set<String> keysetCos = excel_data.keySet();
		rownum = 1;
		for (String key : keysetCos) { // loop through the data and add them to the cell
			Row row = sheetCos.createRow(rownum++);
			Object[] objArr = excel_data.get(key);
			Cell id = row.createCell(0);
			id.setCellValue(rownum - 1);
			int cellnum = 1;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof Long) {
					cell.setCellValue((Long) obj);
				} else if (obj instanceof Double) {
					cell.setCellStyle(doubleStyle);
					cell.setCellValue((Double) obj);
				} else if (obj instanceof Timestamp) {
					cell.setCellStyle(dateTimeStyle);
					cell.setCellValue((Timestamp) obj);
				} else if (obj instanceof Date) {
					cell.setCellStyle(dateStyle);
					cell.setCellValue((Date) obj);
				} else
					cell.setCellValue((String) obj);
			}
		}
		
		FileOutputStream output_file = new FileOutputStream(new File(path, "Report_Baixas_Manuais_" + endDate + ".xlsx")); // create
																														// XLS
																														// file
		new_workbook.write(output_file);// write excel document to output stream
		output_file.close(); // close the file
		new_workbook.close();
		stmt.close();
		conn.close();
		logger.debug("Report Baixas Manuais generated.");

	}
}
