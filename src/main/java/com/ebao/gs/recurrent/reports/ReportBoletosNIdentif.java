package com.ebao.gs.recurrent.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.gs.recurrent.utils.ReportSheet;
import com.ebao.gs.recurrent.utils.ReportsQueries;

public class ReportBoletosNIdentif {
	static Logger logger = LogManager.getLogger(ReportBoletosNIdentif.class.getName());

	public static void generateSheet(Statement stmt, String session, XSSFWorkbook new_workbook, String startDate, String endDate)
			throws SQLException {
		// create a blank workbook object
		XSSFSheet sheet = new_workbook.createSheet("NÃO IDENTIFICADOS"); // create a worksheet with caption
																			// score_details
		/* Create a header to the sheet */
		Row header = sheet.createRow(0);

		ReportSheet.populateHeader(header, new_workbook,
				new String[] { "", "BANK", "BOLETO RECEBIDO", "PRE-PAGAMENTO", "NUMERO DA PROPOSTA", "APOLICE",
						"SEGURADO", "VALOR PAGO", "PAGO EM", "DATA DO CREDITO", "BOLETO ORIGINAL", "VALOR ORIGINAL",
						"VENCIMENTO ORIGINAL", "DEBIT NOTE", "SALDO EM ABERTO", "STATUS", "STATUS SALDO",
						"PERCENTUAL DE IOF", "DESCRICAO DO ERRO", "RUN_ID", "PROCESSADO EM", "IOF_RETIDO_SANTANDER",
						"DESCONTO_AGRAVO_SANTNADER", "FLAG_BAIXA" });

		logger.debug("Query started.");
		stmt.executeQuery("ALTER SESSION SET CURRENT_SCHEMA = " + session);
		ResultSet query_set = stmt.executeQuery(ReportsQueries.getNIdentif_Boletos(startDate, endDate));
		/* Create Map for Excel Data */
		Map<String, Object[]> excel_data = new HashMap<String, Object[]>(); // create a map and define data
		int row_counter = 1;
		/* Populate data into the Map */
		while (query_set.next()) {
			row_counter = row_counter + 1;

			String a = query_set.getString("BANK");
			String b = query_set.getString("BOLETO RECEBIDO");
			String c = query_set.getString("PRE-PAGAMENTO");
			String d = query_set.getString("NUMERO DA PROPOSTA");
			String e = query_set.getString("APOLICE");
			String f = query_set.getString("SEGURADO");
			double g = query_set.getDouble("VALOR PAGO");
			Date h = query_set.getDate("PAGO EM");
			Date i = query_set.getDate("DATA DO CREDITO");
			String j = query_set.getString("BOLETO ORIGINAL");
			double k = query_set.getDouble("VALOR ORIGINAL");
			Date l = query_set.getDate("VENCIMENTO ORIGINAL");
			String m = query_set.getString("DEBIT NOTE");
			String n = query_set.getString("SALDO EM ABERTO");
			String o = query_set.getString("STATUS");
			String p = query_set.getString("STATUS SALDO");
			Float q = query_set.getFloat("PERCENTUAL DE IOF");
			String r = query_set.getString("DESCRICAO DO ERRO");
			Long s = query_set.getLong("BATCH_RUN_ID");
			Timestamp t = query_set.getTimestamp("PROCESSADO EM");
			double u = query_set.getDouble("IOF_RETIDO_SANTANDER");
			double v = query_set.getDouble("DESCONTO_AGRAVO_SANTNADER");
			Long w = query_set.getLong("FLAG_BAIXA");
			excel_data.put(Long.toString(row_counter),
					new Object[] { a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r,s, t, u, v, w });
		}
		/* Close all DB related objects */
		query_set.close();

		CellStyle dateStyle = new_workbook.createCellStyle();
		CreationHelper createHelper = new_workbook.getCreationHelper();
		short dateFormat = createHelper.createDataFormat().getFormat("dd/MM/yyyy");
		dateStyle.setDataFormat(dateFormat);

		CellStyle dateTimeStyle = new_workbook.createCellStyle();
		CreationHelper createTimeHelper = new_workbook.getCreationHelper();
		short dateTimeFormat = createTimeHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss");
		dateTimeStyle.setDataFormat(dateTimeFormat);

		DataFormat format = new_workbook.createDataFormat();
		CellStyle floatStyle = new_workbook.createCellStyle();
		floatStyle.setDataFormat(format.getFormat("0.00000"));
		/* Load data into logical worksheet */
		Set<String> keyset = excel_data.keySet();
		int rownum = 1;
		for (String key : keyset) { // loop through the data and add them to the cell
			Row row = sheet.createRow(rownum++);
			Object[] objArr = excel_data.get(key);
			Cell id = row.createCell(0);
			id.setCellValue(rownum - 1);
			int cellnum = 1;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof Double) {
					cell.setCellValue((Double) obj);
				} else if (obj instanceof Float) {
					cell.setCellStyle(floatStyle);
					cell.setCellValue((Float) obj);
				} else if (obj instanceof Long) {
					cell.setCellValue((Long) obj);
				} else if (obj instanceof Timestamp) {
					cell.setCellStyle(dateTimeStyle);
					cell.setCellValue((Timestamp) obj);
				} else if (obj instanceof Date) {
					cell.setCellStyle(dateStyle);
					cell.setCellValue((Date) obj);
				} else
					cell.setCellValue((String) obj);
			}
		}

	}
}
