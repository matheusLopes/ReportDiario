package com.ebao.gs.recurrent.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.gs.recurrent.jdbc.DatabaseConnection;
import com.ebao.gs.recurrent.jdbc.DatabaseInfo;

public class ReportBoletos {
	static Logger logger = LogManager.getLogger(ReportBoletos.class.getName());

	public static void generateReport(String startDate, String endDate) throws SQLException, IOException, ClassNotFoundException, GeneralSecurityException {
		String session = DatabaseInfo.getSession();
		String path = DatabaseInfo.getOutputDir();
		
		DatabaseConnection db = new DatabaseConnection();
		Connection conn = db.connect(DatabaseInfo.getDatabase(), DatabaseInfo.getUser(), DatabaseInfo.getPass());
		Statement stmt = conn.createStatement();
		
		/* Create Workbook and Worksheet objects */
		XSSFWorkbook new_workbook = new XSSFWorkbook();

		ReportBoletosNormais.generateSheet(stmt, session, new_workbook, startDate, endDate);
		ReportBoletosCancelados.generateSheet(stmt, session, new_workbook, startDate, endDate);
		ReportBoletosPrePag.generateSheet(stmt, session, new_workbook, startDate, endDate);
		ReportBoletosNIdentif.generateSheet(stmt, session, new_workbook, startDate, endDate);
		ReportBoletosGeral.generateSheet(stmt, session, new_workbook, startDate, endDate);
		ReportBoletosSumHsbc.generateSheet(stmt, session, new_workbook, startDate, endDate);
		ReportBoletosSumSant.generateSheet(stmt, session, new_workbook, startDate, endDate);

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		Calendar c1 = Calendar.getInstance();

		FileOutputStream output_file = new FileOutputStream(
				new File(path, "report_boletos_" + sdf.format(c1.getTime()) + ".xlsx")); // create
		// XLS
		// file
		new_workbook.write(output_file);// write excel document to output stream
		output_file.close(); // close the file
		new_workbook.close();	
		stmt.close();
		conn.close();
		logger.debug("Report Boletos generated.");

	}

}
