package com.ebao.gs.recurrent.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.gs.recurrent.jdbc.DatabaseConnection;
import com.ebao.gs.recurrent.jdbc.DatabaseInfo;
import com.ebao.gs.recurrent.utils.ReportSheet;
import com.ebao.gs.recurrent.utils.ReportsQueries;

public class ReportIOF {
	static Logger logger = LogManager.getLogger(ReportIOF.class.getName());

	public static void generateReport(String startDate, String endDate) throws SQLException, IOException, ClassNotFoundException, GeneralSecurityException {
		String session = DatabaseInfo.getSession();
		String path = DatabaseInfo.getOutputDir();
		
		DatabaseConnection db = new DatabaseConnection();
		Connection conn = db.connect(DatabaseInfo.getDatabase(), DatabaseInfo.getUser(), DatabaseInfo.getPass());
		Statement stmt = conn.createStatement();
		
		/* Create Workbook and Worksheet objects */
		XSSFWorkbook new_workbook = new XSSFWorkbook();
		
		// create a blank workbook object
		XSSFSheet sheet = new_workbook.createSheet("SQL_Results"); // create a worksheet with caption score_details
		Row header = sheet.createRow(0);
		ReportSheet.populateHeader(header, new_workbook,new String[] {"", "TYPE_QUERY",	"LOB",
				"Policy_No","Endorsement_or_Declaration_no","Currency","Debit_Note_Installment",
				"RECEIPT_DATE",	"TRANSACTION_DATE",	"Premium", "IOF","RECEIPT_NO",
				"TIPO_BAIXA","PAY_MODE_NAME","DIRECT_ER","MAJOR_LINE_CODE","MINOR_LINE_CODE",
				"MAJOR_LINE_NAME","BARCODE","PER_IOF","BOLETO_CORRESPONDENTE","FLAG_BAIXA"});

		if(startDate == null && endDate == null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Calendar c1 = Calendar.getInstance();
			Calendar c2 = Calendar.getInstance();
	
			int day = c1.get(Calendar.DAY_OF_MONTH);
			if (day >= 1 && day < 11) {
				//Initial date, last day of last month, then set the day to 21
				c1.add(Calendar.DATE, -day);
				c1.set(Calendar.DAY_OF_MONTH, 21);
				//Final date, last day of last month
				c2.add(Calendar.DATE, -day);
				
			} else if (day >= 11 && day < 21) {
				c1.set(Calendar.DAY_OF_MONTH, 1);
				c2.set(Calendar.DAY_OF_MONTH, 10);
			} else {
				c1.set(Calendar.DAY_OF_MONTH, 11);
				c2.set(Calendar.DAY_OF_MONTH, 20);
			}
			//initial hour 00:00:00
	        c1.set(Calendar.SECOND, 0);
	        c1.set(Calendar.MINUTE, 0);
			c1.set(Calendar.HOUR_OF_DAY, 0);
			
			//final hour 23:59:59
	        c2.set(Calendar.SECOND, 59);
	        c2.set(Calendar.MINUTE, 59);
			c2.set(Calendar.HOUR_OF_DAY, 23);
			
			startDate = sdf.format(c1.getTime());
			endDate = sdf.format(c2.getTime());
		}
		
		startDate = startDate.replaceAll("/", "");
		endDate = endDate.replaceAll("/", "");
		
		logger.debug("Startdate: " + startDate + ", Enddate:" + endDate);

		logger.debug("Query started.");
		stmt.executeQuery("ALTER SESSION SET CURRENT_SCHEMA = " + session);
		ResultSet query_set = stmt.executeQuery(ReportsQueries.getIOF(startDate, endDate));
		
		/* Create Map for Excel Data */
		Map<String, Object[]> excel_data = new HashMap<String, Object[]>(); // create a map and define data
		
		// excel_data.put("1", new Object[]
		// {"TYPE_QUERY","LOB","Policy_No","Endorsement_or_Declaration_no","Currency","Debit_Note_Installment","RECEIPT_DATE","TRANSACTION_DATE","Premium","IOF","RECEIPT_NO","TIPO_BAIXA","PAY_MODE_NAME","DIRECT_ER","MAJOR_LINE_CODE","MINOR_LINE_CODE","MAJOR_LINE_NAME","BARCODE","PER_IOF","BOLETO_CORRESPONDENTE"});
		int row_counter = 1;
		
		/* Populate data into the Map */
		while (query_set.next()) {
			row_counter = row_counter + 1;
			String a = query_set.getString("TYPE_QUERY");
			String b = query_set.getString("LOB");
			String c = query_set.getString("Policy_No");
			String d = query_set.getString("Endorsement_or_Declaration_no");
			String e = query_set.getString("Currency");
			String f = query_set.getString("Debit_Note_Installment");
			Date g = query_set.getDate("RECEIPT_DATE");
			Timestamp h = query_set.getTimestamp("TRANSACTION_DATE");
			double i = query_set.getDouble("Premium");
			double j = query_set.getDouble("IOF");
			String k = query_set.getString("RECEIPT_NO");
			String l = query_set.getString("TIPO_BAIXA");
			String m = query_set.getString("PAY_MODE_NAME");
			Float n = query_set.getFloat("DIRECT_ER");
			String o = query_set.getString("MAJOR_LINE_CODE");
			String p = query_set.getString("MINOR_LINE_CODE");
			String q = query_set.getString("MAJOR_LINE_NAME");
			String r = query_set.getString("BARCODE");
			double s = query_set.getDouble("PER_IOF");
			String t = query_set.getString("BOLETO_CORRESPONDENTE");
			Long u = query_set.getLong("FLAG_BAIXA");
			excel_data.put(Integer.toString(row_counter),
					new Object[] { a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u });
		}
		
		/* Close all DB related objects */
		query_set.close();

		CellStyle dateStyle = new_workbook.createCellStyle();
		CreationHelper createHelper = new_workbook.getCreationHelper();
		short dateFormat = createHelper.createDataFormat().getFormat("dd/MM/yyyy");
		dateStyle.setDataFormat(dateFormat);

		CellStyle dateTimeStyle = new_workbook.createCellStyle();
		CreationHelper createTimeHelper = new_workbook.getCreationHelper();
		short dateTimeFormat = createTimeHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss");
		dateTimeStyle.setDataFormat(dateTimeFormat);
		DataFormat format = new_workbook.createDataFormat();
		CellStyle floatStyle = new_workbook.createCellStyle();
		floatStyle.setDataFormat(format.getFormat("0.000000"));
		
		/* Load data into logical worksheet */
		Set<String> keyset = excel_data.keySet();
		int rownum = 1;
		for (String key : keyset) { // loop through the data and add them to the cell
			Row row = sheet.createRow(rownum++);
			Object[] objArr = excel_data.get(key);
			Cell id = row.createCell(0);
			id.setCellValue(rownum - 1);
			int cellnum = 1;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof Long) {
					cell.setCellValue((Long) obj);
				}
				else if (obj instanceof Double) {
					cell.setCellValue((Double) obj);
				}
				else if (obj instanceof Float) {
					cell.setCellStyle(floatStyle);
					cell.setCellValue((Float) obj);
				} else if (obj instanceof Integer) {
					cell.setCellValue((Integer) obj);
				} else if (obj instanceof Timestamp) {
					cell.setCellStyle(dateTimeStyle);
					cell.setCellValue((Timestamp) obj);
				} else if (obj instanceof Date) {
					cell.setCellStyle(dateStyle);
					cell.setCellValue((Date) obj);
				} else
					cell.setCellValue((String) obj);
			}
		}

		FileOutputStream output_file = new FileOutputStream(new File(path, "Report_Boletos_IOF_" + endDate.substring(0, 8) + ".xlsx")); // create
																														// XLS
																														// file
		new_workbook.write(output_file);// write excel document to output stream
		output_file.close(); // close the file
		new_workbook.close();
		stmt.close();
		conn.close();
		logger.debug("Report IOF generated.");

	}

}
