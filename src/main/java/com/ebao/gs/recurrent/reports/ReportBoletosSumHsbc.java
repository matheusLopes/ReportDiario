package com.ebao.gs.recurrent.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.gs.recurrent.utils.ReportSheet;
import com.ebao.gs.recurrent.utils.ReportsQueries;

public class ReportBoletosSumHsbc {
	static Logger logger = LogManager.getLogger(ReportBoletosSumHsbc.class.getName());

	public static void generateSheet(Statement stmt, String session, XSSFWorkbook new_workbook, String startDate, String endDate)
			throws SQLException {
		// create a blank workbook object
		XSSFSheet sheet = new_workbook.createSheet("SUMARIZADO - HSBC"); // create a worksheet with caption score_details
		/* Create a header to the sheet */
		Row header = sheet.createRow(0);

		ReportSheet.populateHeader(header, new_workbook,
				new String[] { "", "STATUS_NAME", "COUNT(1)" });

		logger.debug("Query started.");
		stmt.executeQuery("ALTER SESSION SET CURRENT_SCHEMA = " + session);
		ResultSet query_set = stmt.executeQuery(ReportsQueries.getSumHsbc_Boletos(startDate, endDate));
		/* Create Map for Excel Data */
		Map<String, Object[]> excel_data = new HashMap<String, Object[]>(); // create a map and define data
		int row_counter = 1;
		/* Populate data into the Map */
		while (query_set.next()) {
			row_counter = row_counter + 1;
			String a = query_set.getString("STATUS_NAME");
			Integer b = query_set.getInt("COUNT(1)");
			
			excel_data.put(Integer.toString(row_counter), new Object[] { a, b });
		}
		/* Close all DB related objects */
		query_set.close();

		/* Load data into logical worksheet */
		Set<String> keyset = excel_data.keySet();
		int rownum = 1;
		for (String key : keyset) { // loop through the data and add them to the cell
			Row row = sheet.createRow(rownum++);
			Object[] objArr = excel_data.get(key);
			Cell id = row.createCell(0);
			id.setCellValue(rownum - 1);
			int cellnum = 1;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof Integer) {
					cell.setCellValue((Integer) obj);
				} else
					cell.setCellValue(((String) obj).trim());
			}
		}

	}
}
