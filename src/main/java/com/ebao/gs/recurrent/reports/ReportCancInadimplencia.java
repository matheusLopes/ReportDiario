package com.ebao.gs.recurrent.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.gs.recurrent.jdbc.DatabaseConnection;
import com.ebao.gs.recurrent.jdbc.DatabaseInfo;
import com.ebao.gs.recurrent.utils.ReportSheet;
import com.ebao.gs.recurrent.utils.ReportsQueries;

public class ReportCancInadimplencia {
	static Logger logger = LogManager.getLogger(ReportCancInadimplencia.class.getName());

	public static void generateReport()
			throws SQLException, IOException, ClassNotFoundException, GeneralSecurityException {
		String session = DatabaseInfo.getSession();
		String path = DatabaseInfo.getOutputDir();

		DatabaseConnection db = new DatabaseConnection();
		Connection conn = db.connect(DatabaseInfo.getDatabase(), DatabaseInfo.getUser(), DatabaseInfo.getPass());
		Statement stmt = conn.createStatement();

		/* Create Workbook and Worksheet objects */
		XSSFWorkbook new_workbook = new XSSFWorkbook();
		// create a blank workbook object
		XSSFSheet sheet = new_workbook.createSheet("SQL Results"); // create a worksheet with caption score_details
		/* Create a header to the sheet */
		Row header = sheet.createRow(0);

		ReportSheet.populateHeader(header, new_workbook, new String[] { "", "Número de Apólice", "Moeda",
				"Valor da parcela", "Parcela", "Data de emissão do cancelamento" });

		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		Calendar c1 = Calendar.getInstance();
		String startDate = null;
		String endDate = null;

		int day = c1.get(Calendar.DAY_OF_MONTH);
		c1.add(Calendar.DATE, -day);
		endDate = sdf.format(c1.getTime());
		c1.set(Calendar.DAY_OF_MONTH, 1);
		startDate = sdf.format(c1.getTime());

		logger.debug("Startdate: " + startDate + ", Enddate:" + endDate);

		logger.debug("Query started.");
		stmt.executeQuery("ALTER SESSION SET CURRENT_SCHEMA = " + session);
		ResultSet query_set = stmt.executeQuery(ReportsQueries.getCancelInadim(startDate, endDate));
		/* Create Map for Excel Data */
		Map<String, Object[]> excel_data = new HashMap<String, Object[]>(); // create a map and define data
		int row_counter = 1;
		/* Populate data into the Map */
		while (query_set.next()) {
			row_counter = row_counter + 1;
			String a = query_set.getString("POLICY_NO");
			String b = query_set.getString("MOEDA");
			double c = query_set.getDouble("AMOUNT");
			String d = query_set.getString("CUR_PERIOD");
			Timestamp e = query_set.getTimestamp("ISSUE_DATE");

			excel_data.put(Integer.toString(row_counter), new Object[] { a, b, c, d, e });
		}
		/* Close all DB related objects */
		query_set.close();

		CellStyle dateTimeStyle = new_workbook.createCellStyle();
		CreationHelper createTimeHelper = new_workbook.getCreationHelper();
		short dateTimeFormat = createTimeHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss");
		dateTimeStyle.setDataFormat(dateTimeFormat);
		DataFormat format = new_workbook.createDataFormat();
		CellStyle doubleStyle = new_workbook.createCellStyle();
		doubleStyle.setDataFormat(format.getFormat("0.00"));
		/* Load data into logical worksheet */
		Set<String> keyset = excel_data.keySet();
		int rownum = 1;
		for (String key : keyset) { // loop through the data and add them to the cell
			Row row = sheet.createRow(rownum++);
			Object[] objArr = excel_data.get(key);
			Cell id = row.createCell(0);
			id.setCellValue(rownum - 1);
			int cellnum = 1;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof Double) {
					cell.setCellStyle(doubleStyle);
					cell.setCellValue((Double) obj);
				} else if (obj instanceof Timestamp) {
					cell.setCellStyle(dateTimeStyle);
					cell.setCellValue((Timestamp) obj);
				} else
					cell.setCellValue(((String) obj).trim());
			}
		}

		FileOutputStream output_file = new FileOutputStream(
				new File(path, "CancelamentoInadimplencia_" + endDate + ".xlsx")); // create
		// XLS
		// file
		new_workbook.write(output_file);// write excel document to output stream
		output_file.close(); // close the file
		new_workbook.close();
		stmt.close();
		conn.close();
		logger.debug("Report Cancellation by default generated.");

	}

}
