package com.ebao.gs.recurrent.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.gs.recurrent.utils.ReportSheet;
import com.ebao.gs.recurrent.utils.ReportsQueries;

public class ReportBoletosNormais {
	static Logger logger = LogManager.getLogger(ReportBoletosNormais.class.getName());

	public static void generateSheet(Statement stmt, String session, XSSFWorkbook new_workbook, String startDate, String endDate)
			throws SQLException {
		// create a blank workbook object
		XSSFSheet sheetNormais = new_workbook.createSheet("NORMAIS"); // create a worksheet with caption score_details
		/* Create a header to the sheet */
		Row headerNormais = sheetNormais.createRow(0);

		ReportSheet.populateHeader(headerNormais, new_workbook,
				new String[] { "", "BANK", "APOLICE", "DEBIT NOTE", "BOLETO", "REPRINT?", "GERAÇAO 1º BOLETO",
						"GERAÇAO ÚLTIMO BOLETO", "DATA GERAÇAO BOLETO PAGO", "STATUS DO BOLETO", "VALOR ORIGINAL",
						"VALOR PAGO", "JUROS ACRESC", "JUROS ESPERADO", "SALDO EM ABERTO", "STATUS DA PARCELA",
						"STATUS SALDO", "PARCELA", "VENCIMENTO", "PAGO EM", "DATA DO CREDITO", "CONVENIO", "IOF",
						"PERCENTUAL IOF", "DESCRIÇÃO DO ERRO", "RUN_ID", "PROCESSADO EM", "QUANTIDADE_PAGOS",
						"IOF_RETIDO_SANTANDER", "DESCONTO_AGRAVO_SANTANDER", "FLAG_BAIXA" });

		logger.debug("Query started.");
		stmt.executeQuery("ALTER SESSION SET CURRENT_SCHEMA = " + session);
		ResultSet query_set = stmt.executeQuery(ReportsQueries.queryNormais_Boletos(startDate, endDate));
		/* Create Map for Excel Data */
		Map<String, Object[]> excel_data = new HashMap<String, Object[]>(); // create a map and define data
		int row_counter = 1;
		/* Populate data into the Map */
		while (query_set.next()) {
			row_counter = row_counter + 1;
			String a = query_set.getString("BANK");
			String b = query_set.getString("APOLICE");
			String c = query_set.getString("DEBIT NOTE");
			String d = query_set.getString("BOLETO");
			Long e = query_set.getLong("REPRINT?");
			Timestamp f = query_set.getTimestamp("GERAÇAO 1º BOLETO");
			Timestamp g = query_set.getTimestamp("GERAÇAO ÚLTIMO BOLETO");
			Timestamp h = query_set.getTimestamp("DATA GERAÇAO BOLETO PAGO");
			String i = query_set.getString("STATUS DO BOLETO");
			double j = query_set.getDouble("VALOR ORIGINAL");
			double k = query_set.getDouble("VALOR PAGO");
			double l = query_set.getDouble("JUROS ACRESC");
			double m = query_set.getDouble("JUROS ESPERADO");
			double n = query_set.getDouble("SALDO EM ABERTO");
			String o = query_set.getString("STATUS DA PARCELA");
			String p = query_set.getString("STATUS SALDO");
			Long q = query_set.getLong("PARCELA");
			Date r = query_set.getDate("VENCIMENTO");
			Date s = query_set.getDate("PAGO EM");
			Date t = query_set.getDate("DATA DO CREDITO");
			String u = query_set.getString("CONVENIO");
			double v = query_set.getDouble("IOF");
			Float w = query_set.getFloat("PERCENTUAL IOF");
			String y = query_set.getString("DESCRIÇÃO DO ERRO");
			Long x = query_set.getLong("RUN ID");
			Timestamp z = query_set.getTimestamp("PROCESSADO EM");
			Long aa = query_set.getLong("QUANTIDADE_PAGOS");
			double ab = query_set.getDouble("IOF_RETIDO_SANTANDER");
			double ac = query_set.getDouble("DESCONTO_AGRAVO_SANTANDER");
			Long ad = query_set.getLong("FLAG_BAIXA");
			excel_data.put(Integer.toString(row_counter), new Object[] { a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p,
					q, r, s, t, u, v, w, y, x, z, aa, ab, ac, ad });
		}
		/* Close all DB related objects */
		query_set.close();

		CellStyle dateStyle = new_workbook.createCellStyle();
		CreationHelper createHelper = new_workbook.getCreationHelper();
		short dateFormat = createHelper.createDataFormat().getFormat("dd/MM/yyyy");
		dateStyle.setDataFormat(dateFormat);

		CellStyle dateTimeStyle = new_workbook.createCellStyle();
		CreationHelper createTimeHelper = new_workbook.getCreationHelper();
		short dateTimeFormat = createTimeHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss");
		dateTimeStyle.setDataFormat(dateTimeFormat);
		DataFormat format = new_workbook.createDataFormat();
		/*
		 * CellStyle doubleStyle = new_workbook.createCellStyle();
		 * doubleStyle.setDataFormat(format.getFormat("0.00"));
		 */
		CellStyle floatStyle = new_workbook.createCellStyle();
		floatStyle.setDataFormat(format.getFormat("0.00000"));
		/* Load data into logical worksheet */
		Set<String> keyset = excel_data.keySet();
		int rownum = 1;
		for (String key : keyset) { // loop through the data and add them to the cell
			Row row = sheetNormais.createRow(rownum++);
			Object[] objArr = excel_data.get(key);
			Cell id = row.createCell(0);
			id.setCellValue(rownum - 1);
			int cellnum = 1;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof Double) {
					cell.setCellValue((Double) obj);
				} else if (obj instanceof Float) {
					cell.setCellStyle(floatStyle);
					cell.setCellValue((Float) obj);
				} else if (obj instanceof Long) {
					cell.setCellValue((Long) obj);
				} else if (obj instanceof Timestamp) {
					cell.setCellStyle(dateTimeStyle);
					cell.setCellValue((Timestamp) obj);
				} else if (obj instanceof Date) {
					cell.setCellStyle(dateStyle);
					cell.setCellValue((Date) obj);
				} else
					cell.setCellValue(((String) obj).trim());
			}
		}

	}
}
