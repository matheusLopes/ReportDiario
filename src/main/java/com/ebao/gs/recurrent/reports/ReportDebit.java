package com.ebao.gs.recurrent.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.gs.recurrent.jdbc.DatabaseConnection;
import com.ebao.gs.recurrent.jdbc.DatabaseInfo;
import com.ebao.gs.recurrent.utils.ReportSheet;
import com.ebao.gs.recurrent.utils.ReportsQueries;

public class ReportDebit {
	static Logger logger = LogManager.getLogger(ReportDebit.class.getName());

	public static void generateReport(String startDate, String endDate) throws SQLException, IOException, ClassNotFoundException, GeneralSecurityException {
		String session = DatabaseInfo.getSession();
		String path = DatabaseInfo.getOutputDir();
		
		DatabaseConnection db = new DatabaseConnection();
		Connection conn = db.connect(DatabaseInfo.getDatabase(), DatabaseInfo.getUser(), DatabaseInfo.getPass());
		Statement stmt = conn.createStatement();
		
		/* Create Workbook and Worksheet objects */
		XSSFWorkbook new_workbook = new XSSFWorkbook();
		// create a blank workbook object
		XSSFSheet sheet = new_workbook.createSheet("SQL Results"); // create a worksheet with caption score_details
		/* Create a header to the sheet*/
		Row header = sheet.createRow(0);
		
		ReportSheet.populateHeader(header, new_workbook,new String[] {"","APOLICE","BANK_CODE",
				"AGENCIA_CONTA","DEBIT_NOTE","VALOR_ORIGINAL","VALOR_DEBITADO","NOME_DO_ARQUIVO",
				"PROCESSADO_EM","SEQUENCIAL_NUMBER","COMPANY_CODE","CONVENIO","BANK_BRANCH_CODE",
				"AR_AMOUNT","DEBIT_DATE","PH_BANK_BRANCH_CODE","PH_NAME","RETURN_CODE",
				"RETURN_REASON_CODE","INSERT_TIMESTAMP",});
		
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		Calendar c1 = Calendar.getInstance();
		
		logger.debug("Startdate: " + startDate + ", Enddate:" + endDate);

		logger.debug("Query started.");
		stmt.executeQuery("ALTER SESSION SET CURRENT_SCHEMA = " + session);
		ResultSet query_set = stmt.executeQuery(ReportsQueries.getDebit(startDate, endDate));
		/* Create Map for Excel Data */
		Map<String, Object[]> excel_data = new HashMap<String, Object[]>(); // create a map and define data
		int row_counter = 1;
		/* Populate data into the Map */
		while (query_set.next()) {
			row_counter = row_counter + 1;
			String a 	= query_set.getString("APOLICE");
			String b 	= query_set.getString("BANK_CODE");
			String c 	= query_set.getString("AGENCIA_CONTA");
			String d 	= query_set.getString("DEBIT_NOTE");
			double e 	= query_set.getDouble("VALOR_ORIGINAL");
			double f 	= query_set.getDouble("VALOR_DEBITADO");
			String g 	= query_set.getString("NOME_DO_ARQUIVO");
			Timestamp h = query_set.getTimestamp("PROCESSADO_EM");
			Integer i 	= query_set.getInt("SEQUENCIAL_NUMBER");
			String j 	= query_set.getString("COMPANY_CODE");
			String k 	= query_set.getString("CONVENIO");
			String l 	= query_set.getString("BANK_BRANCH_CODE");
			double m 	= query_set.getDouble("AR_AMOUNT");
			Date n 		= query_set.getDate("DEBIT_DATE");
			String o 	= query_set.getString("PH_BANK_BRANCH_CODE");
			String p 	= query_set.getString("PH_NAME");
			Integer q 	= query_set.getInt("RETURN_CODE");
			String r 	= query_set.getString("RETURN_REASON_CODE");
			Timestamp s = query_set.getTimestamp("INSERT_TIMESTAMP");
			excel_data.put(Integer.toString(row_counter),
					new Object[] { a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s });
		}
		/* Close all DB related objects */
		query_set.close();

		CellStyle dateStyle = new_workbook.createCellStyle();
		CreationHelper createHelper = new_workbook.getCreationHelper();
		short dateFormat = createHelper.createDataFormat().getFormat("dd/MM/yyyy");
		dateStyle.setDataFormat(dateFormat);

		CellStyle dateTimeStyle = new_workbook.createCellStyle();
		CreationHelper createTimeHelper = new_workbook.getCreationHelper();
		short dateTimeFormat = createTimeHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss");
		dateTimeStyle.setDataFormat(dateTimeFormat);
		DataFormat format = new_workbook.createDataFormat();
		CellStyle doubleStyle = new_workbook.createCellStyle();
		doubleStyle.setDataFormat(format.getFormat("0.00"));
		/* Load data into logical worksheet */
		Set<String> keyset = excel_data.keySet();
		int rownum = 1;
		for (String key : keyset) { // loop through the data and add them to the cell
			Row row = sheet.createRow(rownum++);
			Object[] objArr = excel_data.get(key);
			Cell id = row.createCell(0);
			id.setCellValue(rownum - 1);
			int cellnum = 1;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof Double) {
					cell.setCellStyle(doubleStyle);
					cell.setCellValue((Double) obj);
				}
				else if (obj instanceof Float) {					
					cell.setCellValue((Float) obj);
				}
				else if (obj instanceof Integer) {					
					cell.setCellValue((Integer) obj);
				}
				else if (obj instanceof Timestamp) {
					cell.setCellStyle(dateTimeStyle);
					cell.setCellValue((Timestamp) obj);
				} else if (obj instanceof Date) {
					cell.setCellStyle(dateStyle);
					cell.setCellValue((Date) obj);
				} else
					cell.setCellValue(((String) obj).trim());
			}
		}
		
		XSSFSheet sheetStatment = new_workbook.createSheet("SQL Statement"); // create a worksheet with caption score_details
		Row first = sheetStatment.createRow(0);
		first.createCell(0).setCellValue("Levantamento de todos os recebidos de autodebito diário ");
		Row second = sheetStatment.createRow(1);
		second.createCell(0).setCellValue(ReportsQueries.getDebit(startDate,endDate));

		FileOutputStream output_file = new FileOutputStream(new File(path, "report_debito_automatico_" + sdf.format(c1.getTime()) + ".xlsx")); // create
																														// XLS
																														// file
		new_workbook.write(output_file);// write excel document to output stream
		output_file.close(); // close the file
		new_workbook.close();
		stmt.close();
		conn.close();
		logger.debug("Report Debit generated.");

	}

}
